using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Linq;

//using System.IO;
//using System.Runtime.Serialization;
//using System.Runtime.Serialization.Formatters.Binary;

static public class Util {

    public enum OperatorType {
        Equal = 0,
        NotEqual = 1,
        Greater = 2,
        GreaterEqual = 3,
        Less = 4,
        LessEqual = 5,
        And = 6,
        Or = 7
    };

    static public bool VecEqual(Vector3 vec1, Vector3 vec2) {
        return (Mathf.Approximately(vec1.x, vec2.x) && Mathf.Approximately(vec1.y, vec2.y) && Mathf.Approximately(vec1.z, vec2.z));
    }

    static public bool VecEqual(Vector2 vec1, Vector2 vec2) {
        return (Mathf.Approximately(vec1.x, vec2.x) && Mathf.Approximately(vec1.y, vec2.y));
    }

    static public Color RandomColor() {
        return new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));
    }

    static public Color Color255Convert(int r, int g, int b, int a) {
        return new Color(r / 255.0f, g / 255.0f, b / 255.0f, a / 255.0f);
    }

    static public Color Color255Convert(int r, int g, int b) {
        return new Color(r / 255.0f, g / 255.0f, b / 255.0f);
    }

    static public string ColorToHex(Color32 color) {
        string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
        return hex;
    }

    static public Color HexToColor(string hex) {
        if (hex.Length == 6) {
            byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
            byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
            byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
            return new Color32(r, g, b, 255);
        } else {
            return Color.white;
        }
    }

    //Alpha = 0.0f ~ 1.0f
    static public Color HexToColor(string hex, float alpha) {
        if (hex.Length == 6) {
            byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
            byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
            byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
            byte a = (byte)(alpha * 255.0f);
            return new Color32(r, g, b, a);
        } else {
            return new Color(1.0f, 1.0f, 1.0f, alpha); ;
        }
    }

    static public bool RandomBool() {
        if (Random.value >= 0.5f) {
            return true;
        } else {
            return false;
        }
    }

    //Rate = the rate return true (0.0f ~ 1.0f)
    static public bool RandomBool(float rate) {
        if (Random.value < rate) {
            return true;
        } else {
            return false;
        }
    }

    static public int BoolToInt(bool b) {
        return (b) ? (1) : (0);
    }

    static public bool IntToBool(int i) {
        return (i == 0) ? (false) : (true);
    }

    static public int StringToInt(string str) {
        int value = 0;
        int.TryParse(str, out value);
        return value;
    }

    static public bool StringToBool(string str) {
        if (string.Equals(str, "TRUE", System.StringComparison.CurrentCultureIgnoreCase)) {
            return true;
        } else {
            return false;
        }
    }

    public static float StringToFloat(string inputStr) {
        float returnValue = 0.0f;
        float.TryParse(inputStr, out returnValue);
        return returnValue;
    }

    /// <summary>
    /// Reference Article http://www.codeproject.com/KB/tips/SerializedObjectCloner.aspx
    /// Provides a method for performing a deep copy of an object.
    /// Binary Serialization is used to perform the copy.
    /// </summary>
    /// <summary>
    /// Perform a deep Copy of the object.
    /// </summary>
    /// <typeparam name="T">The type of object being copied.</typeparam>
    /// <param name="source">The object instance to copy.</param>
    /// <returns>The copied object.</returns>
    //public static T Clone<T>(T source) {
    //    // Don't serialize a null object, simply return the default for that object
    //    if (Object.ReferenceEquals(source, null)) {
    //        return default(T);
    //    }

    //    IFormatter formatter = new BinaryFormatter();
    //    Stream stream = new MemoryStream();
    //    using (stream) {
    //        formatter.Serialize(stream, source);
    //        stream.Seek(0, SeekOrigin.Begin);
    //        return (T)formatter.Deserialize(stream);
    //    }
    //}

    public static void Shuffle<T>(this List<T> list) {
        System.Random rng = new System.Random();
        int n = list.Count;
        while (n > 1) {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

    //T MUST be a non-container built-in type.
    public static void Resize<T>(this List<T> list, int size, T t) {
        int count = list.Count;
        if (size < count)
            list.RemoveRange(size, count - size);
        else if (size > count) {
            if (size > list.Capacity)//this bit is purely an optimisation, to avoid multiple automatic capacity changes.
                list.Capacity = size;
            list.AddRange(Enumerable.Repeat(t, size - count));
        }
    }

    public static void Resize<T>(this List<T> list, int size) where T : new() {
        Resize(list, size, new T());
    }

    public static byte[] StringToBytes(string str) {
        byte[] bytes = new byte[str.Length * sizeof(char)];
        System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
        return bytes;
    }

    public static string BytesToString(byte[] bytes) {
        char[] chars = new char[bytes.Length / sizeof(char)];
        System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
        return new string(chars);
    }

    public static List<string> SplitByComma(string inputStr) {
        return new List<string>(inputStr.Split(new string[] { "," }, System.StringSplitOptions.RemoveEmptyEntries));
    }

    public static T InstantiateModule<T>(string modulePath, RectTransform parantRectTransform) {
        Object prefab = Resources.Load(modulePath);
        if (prefab != null) {
            GameObject uiGO = GameObject.Instantiate(prefab) as GameObject;
            if (uiGO != null) {
                RectTransform rt = uiGO.GetRectTransform();
                rt.SetParent(parantRectTransform);
                rt.localPosition = Vector3.zero;
                rt.localRotation = Quaternion.identity;
                rt.localScale = Vector3.one;
                rt.sizeDelta = parantRectTransform.sizeDelta;
                uiGO.layer = parantRectTransform.gameObject.layer;

                T returnComponent = uiGO.GetComponent<T>();

                if (returnComponent != null) {
                    return returnComponent;
                } else {
                    Debug.LogError("GetComponent failed!");
                    return default(T);
                }
            } else {
                Debug.LogError("GameObject.Instantiate failed!");
                return default(T);
            }
        } else {
            Debug.LogError("Resources.Load failed!");
            return default(T);
        }
    }

    public static IEnumerator InstantiateModuleAsync<T>(string modulePath, RectTransform parantRectTransform, System.Action<T> onFinish) {
        ResourceRequest resourceRequest = Resources.LoadAsync(modulePath);
        while (!resourceRequest.isDone) {
            yield return null;
        }
        if (resourceRequest.asset != null) {
            GameObject uiGO = GameObject.Instantiate(resourceRequest.asset) as GameObject;
            if (uiGO != null) {
                RectTransform rt = uiGO.GetRectTransform();
                rt.SetParent(parantRectTransform);
                rt.localPosition = Vector3.zero;
                rt.localRotation = Quaternion.identity;
                rt.localScale = Vector3.one;
                rt.sizeDelta = parantRectTransform.sizeDelta;
                uiGO.layer = parantRectTransform.gameObject.layer;

                T returnComponent = uiGO.GetComponent<T>();
                yield return null;

                if (returnComponent != null) {
                    onFinish(returnComponent);
                } else {
                    Debug.LogError("GetComponent failed!");
                    onFinish(default(T));
                }
            } else {
                Debug.LogError("GameObject.Instantiate failed!");
                onFinish(default(T));
            }
        } else {
            Debug.LogError("Resources.LoadAsync failed!");
            onFinish(default(T));
        }
    }

}
