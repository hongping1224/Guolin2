﻿using UnityEngine;

public static class PrototypeExtensions {
    public static void SetLayer(this GameObject go, int layer) {
        go.layer = layer;
        foreach (Transform trans in go.transform)
            trans.gameObject.SetLayer(layer);
    }

    public static GameObject InstantiateChild(this GameObject go, string path, bool resetScale) {
        Object prefab = Resources.Load(path);
        if (prefab != null) {
            GameObject instantiateGO = GameObject.Instantiate(prefab) as GameObject;
            instantiateGO.transform.SetParent(go.transform);
            instantiateGO.layer = go.layer;
            if (resetScale) {
                instantiateGO.transform.localScale = Vector3.one;
            }
            return instantiateGO;
        } else {
            Debug.LogError("Error: Prefab not exist: " + path);
            return null;
        }
    }

    public static GameObject InstantiateChild(this GameObject go, string path, string name, Vector3 localPos, bool resetScale) {
        Object prefab = Resources.Load(path);
        if (prefab != null) {
            GameObject instantiateGO = GameObject.Instantiate(prefab) as GameObject;
            instantiateGO.name = name;
            instantiateGO.transform.SetParent(go.transform);
            instantiateGO.layer = go.layer;
            instantiateGO.transform.localPosition = localPos;
            if (resetScale) {
                instantiateGO.transform.localScale = Vector3.one;
            }
            return instantiateGO;
        } else {
            Debug.LogError("Error: Prefab not exist: " + path);
            return null;
        }
    }

    public static int ToInt(this bool b) {
        return (b) ? (1) : (0);
    }

}