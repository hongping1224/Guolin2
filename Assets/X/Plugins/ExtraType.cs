﻿using System;

namespace X {

    //We need this for serizlize a vector.
    [Serializable]
    public class Vec3 {

        public float x = 0.0f;
        public float y = 0.0f;
        public float z = 0.0f;

        public Vec3() {

        }

        public Vec3(UnityEngine.Vector3 vec3) {
            x = vec3.x;
            y = vec3.y;
            z = vec3.z;
        }

        public UnityEngine.Vector3 ToUnityVector3() {
            return new UnityEngine.Vector3(x, y, z);
        }

        override public string ToString() {
            return "(" + x.ToString() + ", " + y.ToString() + ", " + z.ToString() + ")";
        }

    }

    //We need this for serizlize a vector.
    [Serializable]
    public class Vec2 {

        public float x = 0.0f;
        public float y = 0.0f;

        public Vec2() {

        }

        public Vec2(UnityEngine.Vector2 vec2) {
            x = vec2.x;
            y = vec2.y;
        }

        public UnityEngine.Vector2 ToUnityVector2() {
            return new UnityEngine.Vector2(x, y);
        }

        override public string ToString() {
            return "(" + x.ToString() + ", " + y.ToString() + ")";
        }

    }
}