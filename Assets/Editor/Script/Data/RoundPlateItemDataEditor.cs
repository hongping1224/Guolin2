﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
[CustomEditor(typeof(RoundPlateItemDataList))]
public class RoundPlateItemDataEditor : Editor
{
    [MenuItem("HP/Data/RoundPlateItemData/NewRoundPlateItemDataList")]
    public static void NewFruitDataList()
    {
        RoundPlateItemDataList data = FarmUpgradeDataList.CreateInstance<RoundPlateItemDataList>();
        string folder = Path.GetDirectoryName(RoundPlateItemDataList.FilePath);
        if (Directory.Exists(folder) == false)
        {
            Directory.CreateDirectory(folder);
        }
        if (File.Exists(RoundPlateItemDataList.FilePath))
        {
            if (EditorUtility.DisplayDialog("RoundPlateItemData Already Exist!", "There are already a RoundPlateItemData!! ", "Ok"))
            {
                Selection.activeObject = AssetDatabase.LoadMainAssetAtPath(RoundPlateItemDataList.FilePath);
                EditorGUIUtility.PingObject(Selection.activeObject);
                return;
            }
        }
        AssetDatabase.CreateAsset(data, RoundPlateItemDataList.FilePath);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Clear XID 0 Obj"))
        {
            RoundPlateItemDataList.g_Self.TidyList();
            EditorUtility.SetDirty(RoundPlateItemDataList.g_Self);
        }
        if (GUILayout.Button("Sort List"))
        {
            RoundPlateItemDataList.g_Self.Sort();
            EditorUtility.SetDirty(RoundPlateItemDataList.g_Self);
        }

        if (GUILayout.Button("Add 1 Object"))
        {
            RoundPlateItemDataList.g_Self.AddItem();
            EditorUtility.SetDirty(RoundPlateItemDataList.g_Self);
        }
        if (GUILayout.Button("Add Data From FruitDataList"))
        {
           foreach(FruitData f in FruitDataList.g_Self.Datas)
           {
                bool flag = false;
                List<RoundPlateItemData> data = RoundPlateItemDataList.g_Self.Datas;
                for (int i=0;i < data.Count ; ++i)
                {
                    if (data[i].XID == f.XID)
                    {
                        RoundPlateItemDataList.g_Self.RemoveItem(i);
                        RoundPlateItemData t = new RoundPlateItemData();
                        t.SetXID(f.XID);
                        t.SetSprite(f.SeedSprite);
                        t.SetTag(RoundPlateItemData.RoundPlateItemTag.Seed);
                        RoundPlateItemDataList.g_Self.AddItem(t);
                        flag = true;
                        break;
                    }
                }
                if (flag == false)
                {
                    RoundPlateItemData t = new RoundPlateItemData();
                    t.SetXID(f.XID);
                    t.SetSprite(f.SeedSprite);
                    t.SetTag(RoundPlateItemData.RoundPlateItemTag.Seed);
                    RoundPlateItemDataList.g_Self.AddItem(t);
                }
            }
            EditorUtility.SetDirty(RoundPlateItemDataList.g_Self);
        }
    }

}
