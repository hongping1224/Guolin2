﻿using UnityEditor;
using System.IO;

public class ConfigDataEditor{

    [MenuItem("HP/Data/Config/GenerateConfigData")]
    public static void CreateNewConfigData()
    {
        Config configData = Config.CreateInstance<Config>();
        configData.WaterLifeSpan = 20;
        configData.FertilizerLifeSpan = 20;
        string folder = Path.GetDirectoryName(Config.FilePath);
        if (Directory.Exists(folder) == false)
        {
            Directory.CreateDirectory(folder);
        }
        if (File.Exists(Config.FilePath))
        {
          if(EditorUtility.DisplayDialog("Config Already Exist!","There are already a ConfigData, Do you want to ovveride it?? ", "Yes", "No"))
            {
                AssetDatabase.CreateAsset(configData, Config.FilePath);
                return;
            }else
            {
                return;
            }
        }
        AssetDatabase.CreateAsset(configData, Config.FilePath);
    }
}
