﻿using UnityEngine;
using System.IO;
using UnityEditor;
[CustomEditor(typeof(FarmUpgradeDataList))]
public class FarmUpgradeDataEditor : Editor
{
    [MenuItem("HP/Data/FarmUpgradeData/NewFarmUpgradeDataList")]
    public static void NewFruitDataList()
    {
        FarmUpgradeDataList data = FarmUpgradeDataList.CreateInstance<FarmUpgradeDataList>();
        string folder = Path.GetDirectoryName(FarmUpgradeDataList.FilePath);
        if (Directory.Exists(folder) == false)
        {
            Directory.CreateDirectory(folder);
        }
        if (File.Exists(FarmUpgradeDataList.FilePath))
        {
            if (EditorUtility.DisplayDialog("FarmUpgradeData Already Exist!", "There are already a FarmUpgradeData!! ", "Ok"))
            {
                Selection.activeObject = AssetDatabase.LoadMainAssetAtPath(FarmUpgradeDataList.FilePath);
                EditorGUIUtility.PingObject(Selection.activeObject);
                return;
            }
        }
        AssetDatabase.CreateAsset(data, FarmUpgradeDataList.FilePath);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Clear XID 0 Obj"))
        {
            FarmUpgradeDataList.g_Self.TidyList();
            EditorUtility.SetDirty(FarmUpgradeDataList.g_Self);
        }

        if (GUILayout.Button("Add 1 Object"))
        {
            FarmUpgradeDataList.g_Self.AddItem();
            EditorUtility.SetDirty(FarmUpgradeDataList.g_Self);
        }
    }

}
