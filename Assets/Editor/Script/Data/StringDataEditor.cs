﻿using UnityEngine;
using System.IO;
using UnityEditor;
[CustomEditor(typeof(StringDataList))]
public class StringDataEditor : Editor
{
    [MenuItem("HP/Data/StringData/NewStringDataList")]
    public static void NewStringDataList()
    {
        StringDataList data = StringDataList.CreateInstance<StringDataList>();
        string folder = Path.GetDirectoryName(StringDataList.FilePath);
        if (Directory.Exists(folder) == false)
        {
            Directory.CreateDirectory(folder);
        }
        if (File.Exists(StringDataList.FilePath))
        {
            if (EditorUtility.DisplayDialog("StringData Already Exist!", "There are already a StringData!! ", "Ok"))
            {
                Selection.activeObject = AssetDatabase.LoadMainAssetAtPath(StringDataList.FilePath);
                EditorGUIUtility.PingObject(Selection.activeObject);
                return;
            }
        }
        AssetDatabase.CreateAsset(data, StringDataList.FilePath);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Clear XID 0 Obj"))
        {
            StringDataList.g_Self.TidyList();
            EditorUtility.SetDirty(StringDataList.g_Self);
        }

        if (GUILayout.Button("Add 1 Object"))
        {
            StringDataList.g_Self.AddItem();
            EditorUtility.SetDirty(StringDataList.g_Self);
        }
    }

}
