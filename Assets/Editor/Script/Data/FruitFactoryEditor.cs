﻿using UnityEditor;
using System.IO;
using System;
using UnityEngine;
using System.Text;

[CustomEditor(typeof(FruitDataList))]
public class FruitFactoryEditor :Editor{

    [MenuItem("HP/Data/FruitData/GenerateFruitFactory")]
    public static void GenerateFruitFactory()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append(@"using UnityEngine;");
        sb.Append("\nusing System.Collections;\npublic static class FruitGirlFactory\n{\npublic static FruitGirl Generate(uint i)\n{\nFruitGirl fg;\nswitch (i)\n{\n");

       foreach(FruitData d in FruitDataList.g_Self.Datas)
        {
            if(d.XID !=0 && d.ClassName !="")
            sb.Append("case "+ d.XID + ":\n fg = new "+ d.ClassName +"();\n fg.Ini(); \n return fg;");
        }
        
        sb.Append("default:\nfg = new FruitGirl();\nfg.Ini();\nreturn fg;\n}\n}\n}\n");
        //   FileStream F = new FileStream(Application.dataPath + "/Script/tt.cs", FileMode.Create);
        File.WriteAllText(Application.dataPath + "/Script/FruitGirl/Base/FruitGirlFactory.cs", sb.ToString(), Encoding.UTF8);
    }

    [MenuItem("HP/Data/FruitData/NewFruitDataList")]
    public static void NewFruitDataList()
    {
        FruitDataList data = FruitDataList.CreateInstance<FruitDataList>();
        string folder = Path.GetDirectoryName(FruitDataList.FilePath);
        if (Directory.Exists(folder) == false)
        {
            Directory.CreateDirectory(folder);
        }
        if (File.Exists(FruitDataList.FilePath))
        {
            if (EditorUtility.DisplayDialog("FruitData Already Exist!", "There are already a FruitData!! ", "Ok"))
            {
                Selection.activeObject = AssetDatabase.LoadMainAssetAtPath(FruitDataList.FilePath);
                EditorGUIUtility.PingObject(Selection.activeObject);
                return;
            }
        }
        AssetDatabase.CreateAsset(data, FruitDataList.FilePath);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Clear XID 0 Obj"))
        {
            FruitDataList.g_Self.TidyList();
            EditorUtility.SetDirty(FruitDataList.g_Self);
        }

        if (GUILayout.Button("Add 1 Object"))
        {
            FruitDataList.g_Self.AddItem();
            EditorUtility.SetDirty(FruitDataList.g_Self);
        }

        if (GUILayout.Button("Rebuild Factory"))
        {
            GenerateFruitFactory();
        }


    }
}
