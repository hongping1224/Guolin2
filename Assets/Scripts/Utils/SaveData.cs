﻿using UnityEngine;
using System.Collections;
using System.IO;
public class SaveData<T>
{
    protected static string m_fileLocation = "";
    public static T Load()
    {
        return default(T);
    }

    public virtual void Save()
    {

    }

    public static bool ClearFile()
    {
        Debug.Log(m_fileLocation);
        if (File.Exists(m_fileLocation))
        {
            File.Delete(m_fileLocation);
            return true;
        }
        return false;
    }
}
