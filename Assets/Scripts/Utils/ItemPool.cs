﻿using UnityEngine;
using System.Collections.Generic;

// This Class Is for All Resource Load operation
public static class ItemPool{
    //Dictionary for Look up Obj TO prevent Multiple Expensive Resource.Load Action.
    private static Dictionary<string, GameObject> pool; 

    public static GameObject Generate(string Path)
    {
        //Lazy Loading Dictionary
        if (pool == null)
        {
            pool = new Dictionary<string, GameObject>();
        }
        // try look up in pool
        GameObject ori;
        if (!pool.TryGetValue(Path, out ori))
        {
            // Load in obj if obj not in pool
            ori = Resources.Load<GameObject>(Path);
            pool.Add(Path, ori);
        }
       return GameObject.Instantiate<GameObject>(ori);
    }

}
