﻿using UnityEngine;
using System.Collections;

public class SingletonBase<T> : MonoBehaviour where T : SingletonBase<T> 
{
   private static T _self;
   public static T Self
    {
        get
        {
            if (_self == null)
            {
                //try to get In Scene
                T tmp = MainWindow.gSelf.SingletonObject.GetComponent<T>();
                if (tmp == null)
                {
                    tmp = MainWindow.gSelf.SingletonObject.AddComponent<T>();
                    tmp.Initial();
                }
                _self = tmp;
            }
            return _self;
        }
        set { _self = value; }
    }

    protected void Dispose()
    {
        if (_self != null)
        {
            //try to get In Scene
            T tmp = MainWindow.gSelf.SingletonObject.GetComponent<T>();
            if (tmp != null)
            {
                Destroy(tmp);
            }
            _self = null;
        }
    }

    public virtual void OnDisable()
    {
        Self.Dispose();
    }
    public virtual bool Initial() {
        return true;
    }

}

