﻿using UnityEngine;
using System.Collections;
using System;
public class BaseObject : MonoBehaviour {
    public bool isInit = false;
    public static T CreateObj<T>(Transform parent, string Path, bool autoinit = true) where T : BaseObject {
        return CreateObj<T>(parent, Path, Vector3.zero, autoinit);
    }

    public static T CreateObj<T>(Transform parent, string Path, Vector3 pos, bool autoinit = true) where T : BaseObject {
        GameObject tmp = ItemPool.Generate(Path);
        tmp.transform.Init(parent);
        tmp.transform.localPosition = pos;
        T obj = tmp.GetComponent<T>();
        if (obj == null) {
            throw new System.NullReferenceException("Object In Path :\"" + Path + " \" Don't Have Component :" + typeof(T).ToString());
        }
        if (autoinit)
            obj.Init();
        return obj;
    }

    public virtual void DestroySelf() {
        GameObject.DestroyImmediate(this.gameObject);
    }

    public virtual void Init() {
        if (isInit) {
            return;
        }
        isInit = true;
    }

    public void DestroyAfter(float Second) {
        Invoke("DestroySelf", Second);
    }

}

public class BaseWindow<T> : BaseObject where T : BaseWindow<T> {
    protected static T gself;
    public static T gSelf {
        get { return gself; }
        set { gself = value; }
    }
    public static T Create(Transform parent, string Path) {
        if (gself == null)
            gself = CreateObj<T>(parent, Path);
        return gself;
    }

    public static IEnumerator CreateWinAsync<K>(Transform parent, string Path, Vector3 pos, Action<K> OnFinish, Action<int> OnLoading = null) where K : BaseWindow<K> {
        if (BaseWindow<K>.gself != null) {
            if (OnFinish != null)
                OnFinish(BaseWindow<K>.gself);
        } else {

            ResourceRequest rq = Resources.LoadAsync(Path);
            while (!rq.isDone) {
                if (OnLoading != null)
                    OnLoading((int)(rq.progress + 1));
                yield return null;
            }
            if (rq.asset != null) {
                GameObject uiGO = GameObject.Instantiate(rq.asset) as GameObject;
                RectTransform rt = uiGO.GetRectTransform();
                rt.SetParent(parent);
                rt.localPosition = Vector3.zero;
                rt.localRotation = Quaternion.identity;
                rt.localScale = Vector3.one;
                K returnComponent = uiGO.GetComponent<K>();
                BaseWindow<K>.gself = returnComponent;
                yield return null;

                if (returnComponent != null) {
                    if (OnFinish != null)
                        OnFinish(returnComponent);
                } else {
                    Debug.LogError("GetComponent failed!");
                    if (OnFinish != null)
                        OnFinish(default(K));
                }
            } else {
                Debug.LogError("GameObject.Instantiate failed!");
                if (OnFinish != null)
                    OnFinish(default(K));
            }
        }
    }
}