﻿using UnityEngine;
using System.Collections;

public static class TransformExtention
{
    public static void DestroyChildren(this Transform root)
    {
        int childs = root.childCount;
        for (int i = childs - 1; i >= 0; i--)
        {
            GameObject.Destroy(root.GetChild(i).gameObject);
        }
    }
    public static void InitPositionAndScale(this Transform root)
    {
        root.localPosition = Vector3.zero;
        root.localScale = Vector3.one;
    }
    public static void Init(this Transform root , Transform parent)
    {
        root.SetParent(parent);
        root.InitPositionAndScale();
    }
}
