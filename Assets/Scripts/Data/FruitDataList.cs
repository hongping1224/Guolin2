﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public struct FruitData 
{
    public uint XID;
    public uint FruitNameStringID;
    public string ClassName;
    public uint SkillNameStringID;
    public uint SkillDescriptionStringID;
    public uint SkillEffectStringID;
    public Sprite SeedSprite;
    public uint EvolveTo;
  /*public Sprite SproutSprite;
    public Sprite MatureSprite; */

    public bool IsEmpty()
    {
        if(XID == 0)
        {
            return true;
        }
        return false;
    }

}

[System.Serializable]
public class FruitDataList : DataList<FruitData,FruitDataList>
{
    public const string FilePath = @"Assets/Resources/"+ResourcesPath.DATA + "FruitData.asset";
    public const string ResFilePath = ResourcesPath.DATA + "FruitData";

    public override void LazyLoad()
    {
        _self = Resources.Load<FruitDataList>(ResFilePath);
    }

    public override void TidyList()
    {
        List<FruitData> fruit = new List<FruitData>();
        foreach (FruitData f in Datas)
        {
            if (f.XID != 0 && f.ClassName != "")
            {
                fruit.Add(f);
            }
        }
        Datas = fruit;
    }
    public override Dictionary<uint, FruitData> ToDictionary()
    {
        Dictionary<uint, FruitData> dic = new Dictionary<uint, FruitData>();
        foreach(FruitData f in Datas)
        {
            if(!dic.ContainsKey(f.XID))
            {
                FruitData fata = f;
                dic.Add(f.XID, fata);
            }
            else
            {
                Debug.LogWarning("key already exsit! key : "+ f.XID +" Class : " + f.ClassName);
            }
        }
        return dic;
    }

}


[System.Serializable]
public class FruitUnlockSave : SaveData<FruitUnlockSave>
{
    [System.NonSerialized]
    protected static string m_fileLocation = Application.persistentDataPath + "/FruitUnlock.xml";
    public bool[] Unlocks = new bool[0];
    public uint[] XID = new uint[0];
    public override void Save()
    {
        XmlManager.Save<FruitUnlockSave>(this, m_fileLocation);
    }



    public void Init()
    {
        FruitData[] data = GameDataManager.GetFruitDatas();
        int size = data.Length;
        Unlocks = new bool[size];
        XID = new uint[size];
        int i = 0;
        foreach (FruitData d in data)
        {
            Unlocks[i] = false;
            XID[i] = d.XID;
            i++;
        }
        Unlocks[0] = true;
    }


    new public static FruitUnlockSave Load()
    {
        FruitUnlockSave save = XmlManager.Load<FruitUnlockSave>(m_fileLocation);
        FruitUnlockSave check = new FruitUnlockSave();
        check.Init();
        //copy saved to check
        for (int i = 0; i < save.XID.Length; i++)
        {
            //Something is unlock
            if (save.Unlocks[i])
            {
                uint xid = save.XID[i];
                //FindOutSameXID inCheck And Copy Setting;
                for (int j = 0; j < check.XID.Length; j++)
                {
                    if (check.XID[j] == xid)
                    {
                        check.Unlocks[j] = true;
                        break;
                    }
                }
            }
        }
        return check;
    }

    public bool IsUpgradeUnlock(uint upgrade)
    {
        for (int i = 0; i < XID.Length; i++)
        {
            if (XID[i] == upgrade)
            {
                return Unlocks[i];
            }
        }
        return false;
    }

    public bool Unlock(uint upgrade)
    {
        for (int i = 0; i < XID.Length; i++)
        {
            if (XID[i] == upgrade)
            {
                if (!Unlocks[i])
                {
                    Unlocks[i] = true;
                    Save();
                    return true;
                }
                break;
            }
        }
        return false;
    }

    public List<uint> GetAllUnlocked() {
        List<uint> unlocked = new List<uint>();
        for(int i = 0; i < XID.Length; ++i) {
            if (Unlocks[i]) {
                unlocked.Add(XID[i]);
            }
        }
        return unlocked;
    }
}