﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public struct FarmUpgradeData
{
    public uint XID;
    public uint Require;
    public uint UpgradeNameStringID;
    public uint EffectStringID;
    public uint DescriptionStringID;
    public Sprite ShopSprite;
    public int Price;
    public FarmUpgradeSkill.FarmUpgradeSkillType SkillType;
    public int value;
    public int target;
}

public class FarmUpgradeDataList : DataList<FarmUpgradeData,FarmUpgradeDataList>
{
    [System.NonSerialized]
    public const string FilePath = @"Assets/Resources/" + ResourcesPath.DATA + "FarmUpgradeData.asset";
    [System.NonSerialized]
    public const string ResFilePath = ResourcesPath.DATA + "FarmUpgradeData";

    public override void LazyLoad()
    {
        _self = Resources.Load<FarmUpgradeDataList>(ResFilePath);
    }

    public override void TidyList()
    {
        List<FarmUpgradeData> list = new List<FarmUpgradeData>();
        foreach (FarmUpgradeData f in Datas)
        {
            if (f.XID != 0)
            {
                list.Add(f);
            }
        }
        Datas = list;
    }
    public override Dictionary<uint, FarmUpgradeData> ToDictionary()
    {
        Dictionary<uint, FarmUpgradeData> dic = new Dictionary<uint, FarmUpgradeData>();
        foreach (FarmUpgradeData f in Datas)
        {
            if (!dic.ContainsKey(f.XID))
            {
                FarmUpgradeData fata = f;
                dic.Add(f.XID, fata);
            }
            else
            {
                Debug.LogWarning("key already exsit! key : " + f.XID);
            }
        }
        return dic;
    }
}

[System.Serializable]
public class FarmUpgradeSave : SaveData<FarmUpgradeSave>
{
    [System.NonSerialized]
    protected static string m_fileLocation = Application.persistentDataPath + "/FarmUpgrade.xml";

    public bool[] Upgrades=new bool[0];
    public uint[] XID = new uint[0];
    public override void Save()
    {
        XmlManager.Save<FarmUpgradeSave>(this, m_fileLocation);
    }

  

    public void Init()
    {
        FarmUpgradeData[]  data= GameDataManager.GetFarmUpgradeDatas();
        int size = data.Length;
        Upgrades = new bool[size];
        XID = new uint[size];
        int i = 0;
        foreach(FarmUpgradeData d in data)
        {
            Upgrades[i] = false;
            XID[i] = d.XID;
            i++;
        }
    }


    new public static FarmUpgradeSave Load()
    {
        FarmUpgradeSave save = XmlManager.Load<FarmUpgradeSave>(m_fileLocation);
        FarmUpgradeSave check = new FarmUpgradeSave();
        check.Init();
        //copy saved to check
        for (int i = 0; i < save.XID.Length; i++)
        {
            //Something is unlock
            if(save.Upgrades[i])
            {
                uint xid = save.XID[i];
                //FindOutSameXID inCheck And Copy Setting;
                for (int j = 0; j < check.XID.Length;    j++)
                {
                    if(check.XID[j] == xid)
                    {
                        check.Upgrades[j] = true;
                        break;
                    }
                }
            }
        }
        return check;
    }

    public bool IsUpgradeUnlock(uint upgrade)
    {
        for (int i = 0; i < XID.Length; i++)
        {
            if (XID[i] == upgrade)
            {
                return Upgrades[i];
            }
        }
        return false;
    }

    public bool Unlock(uint upgrade)
    {
        for (int i = 0; i < XID.Length; i++)
        {
            if (XID[i] == upgrade)
            {
                if (!Upgrades[i])
                {
                    Upgrades[i] = true;
                    Save();
                    return true;
                }
                break;
            }
        }
        return false;
    }
}