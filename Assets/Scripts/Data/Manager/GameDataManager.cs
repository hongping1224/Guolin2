﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public static class GameDataManager
{
    private static Dictionary<uint, FruitData> _fruitCol;
    private static Dictionary<uint, FarmUpgradeData> _farmUpgradeCol;
    private static Dictionary<uint, StringData> _stringCol;
    private static Dictionary<uint, RoundPlateItemData> _roundPlateItemCol;

    public static Config GameConfig
    {
        get { return Config.g_Self; }
    }

    public static void Initial()
    {
        _fruitCol = FruitDataList.g_Self.ToDictionary();
        _farmUpgradeCol = FarmUpgradeDataList.g_Self.ToDictionary();
        _stringCol = StringDataList.g_Self.ToDictionary();
        _roundPlateItemCol = RoundPlateItemDataList.g_Self.ToDictionary();
    }

    public static FruitData GetFruitData(uint XID)
    {
        if(_fruitCol == null) { Initial(); }
        FruitData f;
        if (_fruitCol.TryGetValue(XID, out f))
        {
            return f;
        }
        return new FruitData();
    }
    public static FruitData[] GetFruitDatas()
    {
        if (_fruitCol == null) { Initial(); }

        return new List<FruitData>(_fruitCol.Values).ToArray();
    }

    public static StringData GetStringData(uint XID)
    {
        if(_stringCol == null) { Initial(); }
        StringData f;
        if (_stringCol.TryGetValue(XID, out f))
        {
            return f;
        }
        return new StringData();
    }

    public static FarmUpgradeData GetFarmUpgradeData(uint XID)
    {
        if (_farmUpgradeCol == null) { Initial(); }
        FarmUpgradeData f;
        if (_farmUpgradeCol.TryGetValue(XID, out f))
        {
            return f;
        }
        return new FarmUpgradeData();
    }
    public static FarmUpgradeData[] GetFarmUpgradeDatas()
    {
        if (_farmUpgradeCol == null) { Initial(); }
       
        return new List<FarmUpgradeData>(_farmUpgradeCol.Values).ToArray();
    }


    public static RoundPlateItemData GetRoundPlateItemData(uint XID)
    {
        if (_roundPlateItemCol == null) { Initial(); }
        RoundPlateItemData f;
        if (_roundPlateItemCol.TryGetValue(XID, out f))
        {
            return f;
        }
        Debug.LogWarning(XID + " XID Not Exist");
        return new RoundPlateItemData();
    }
    public static RoundPlateItemData[] GetRoundPlateItemDatas()
    {
        if (_roundPlateItemCol == null) { Initial(); }

        return new List<RoundPlateItemData>(_roundPlateItemCol.Values).ToArray();
    }

}
