﻿using UnityEngine;
using System.Collections;

public static class PlayerDataManager
{
    private static FarmUpgradeSave _farmUpgrade;
    private static FruitUnlockSave _fruitUnlock;
    private static PlayerSaveData _saveData;

    public static FarmUpgradeSave FarmUpgrade
    {
        get
        {
            if (_farmUpgrade == null)
                _farmUpgrade = FarmUpgradeSave.Load();
            return _farmUpgrade;
        }
    }
    public static FruitUnlockSave FruitUnlock
    {
        get
        {
            if (_fruitUnlock == null)
                _fruitUnlock = FruitUnlockSave.Load();
            return _fruitUnlock;
        }
    }
    public static PlayerSaveData SaveData
    {
        get
        {
            if (_saveData == null)
                _saveData = PlayerSaveData.Load();
                return _saveData;
        }
    }

    public static void Initial()
    {
        if (_farmUpgrade == null)
            _farmUpgrade = FarmUpgradeSave.Load();
        if (_fruitUnlock == null)
            _fruitUnlock = FruitUnlockSave.Load();
        if (_saveData == null)
            _saveData = PlayerSaveData.Load();
    }
}
