﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Config : ScriptableObject
{
    [System.NonSerialized]
    public const string FilePath = @"Assets/Resources/"+ResourcesPath.DATA+"ConfigData.asset";
    private static Config _self;

    public static Config g_Self {
        get
        {
            //lazy loading
            if(_self ==null)
            {
                _self = Resources.Load<Config>(ResourcesPath.DATA +"ConfigData");
                if (_self ==null)// no data
                {
                    Debug.LogError("there are no predefine config!!");
                }
            }
            return _self;
        }
    }

    // plug out to watering item upgrade 
    //---------------------------------
    public int WaterStackSize = 1;
    public int FertiStackSize = 1;
    //---------------------------------
    public int WaterRegenTime = 30;
    public int FertiRegenTime = 30;
    public int WaterLifeSpan=20;
    public int FertilizerLifeSpan = 20;
    public int WaterGrowRate=1;
    public int FertiGrowRate=1;
    public int OtherGrowRate=0;
    public int BugSpawnRate=5;
    public int LinkNum=3;
    public int ItemQueueNum=2;
    public int FruitMatureTime=10;
    public int FruitSproutTime=6;
    public int FarmWidth=4;
    public int FarmHeight=4;
    public ItemSpawnPair[] ItemChain= new ItemSpawnPair[] { new ItemSpawnPair(1000,1), new ItemSpawnPair(2000, 1), new ItemSpawnPair(1,20) };
}
