﻿using UnityEngine;
using System;
using System.Collections.Generic;

[System.Serializable]
public struct RoundPlateItemData :IComparable<RoundPlateItemData>
{
    public uint XID;
    public RoundPlateItemTag Tag;
    public Sprite Sprite;
    public int amount;

    public bool IsEmpty()
    {
        if (XID == 0)
        {
            return true;
        }
        return false;
    }
    public void SetSprite(Sprite sp)
    {
        Sprite = sp;
    }
    public void SetTag(RoundPlateItemTag sp)
    {
        Tag = sp;
    }
    public void SetXID(uint sp)
    {
        XID = sp;
    }

    public int CompareTo(RoundPlateItemData other)
    {
        return this.XID.CompareTo(other.XID);
    }

    public enum RoundPlateItemTag
    {
        Seed,
        Water,
        Ferti,
        Block,
        Showel,
    }
    public bool IsTool() {
        switch (Tag) {
            case RoundPlateItemTag.Water:
                return true;
            case RoundPlateItemTag.Ferti:
                return true;
            default:
                return false;
        }
    }


    }

    [System.Serializable]
public class RoundPlateItemDataList : DataList<RoundPlateItemData, RoundPlateItemDataList>
{
    public const string FilePath = @"Assets/Resources/" + ResourcesPath.DATA + "RoundPlateItemData.asset";
    public const string ResFilePath = ResourcesPath.DATA + "RoundPlateItemData";

    public override void LazyLoad()
    {
        _self = Resources.Load<RoundPlateItemDataList>(ResFilePath);
    }

    public override void TidyList()
    {
        List<RoundPlateItemData> fruit = new List<RoundPlateItemData>();
        foreach (RoundPlateItemData f in Datas)
        {
            if (f.XID != 0)
            {
                fruit.Add(f);
            }
        }
        Datas = fruit;
    }
    public override Dictionary<uint, RoundPlateItemData> ToDictionary()
    {
        Dictionary<uint, RoundPlateItemData> dic = new Dictionary<uint, RoundPlateItemData>();
        foreach (RoundPlateItemData f in Datas)
        {
            if (!dic.ContainsKey(f.XID))
            {
                RoundPlateItemData fata = f;
                dic.Add(f.XID, fata);
            }
            else
            {
                Debug.LogWarning("key already exsit! key : " + f.XID + " Class : " + f.Tag);
            }
        }
        return dic;
    }
    public void Sort()
    {
        Datas.Sort();
    }

    
}
