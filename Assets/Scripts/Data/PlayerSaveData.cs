﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class PlayerSaveData : SaveData<PlayerSaveData>
{
    public uint EquipingFruitGirl;
    public int Gold;
    public List<ItemData> ItemBag;

    [System.NonSerialized]
    protected static string m_fileLocation = Application.persistentDataPath + "/PlayerData.xml";
    public override void Save()
    {
        XmlManager.Save<PlayerSaveData>(this, m_fileLocation);
    }

    new public static PlayerSaveData Load()
    {
        PlayerSaveData save;
        //Reset Save if Load Fail
        if (!XmlManager.TryLoad<PlayerSaveData>(m_fileLocation, out save))
        {
            Debug.LogWarning("Fail To Load Player Save");
            save.Reset();
        }
        return save;
    }

    public void Reset()
    {
        Debug.LogWarning("Reset Save DATA!!");
        EquipingFruitGirl = 2;
        Gold = 100;
        ItemBag = new List<ItemData>();
    }

}

[System.Serializable]
public class ItemData
{
    public uint XID;
    public int Amount;
}
