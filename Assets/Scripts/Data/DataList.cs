﻿using UnityEngine;
using System.Collections.Generic;


[System.Serializable]
public class DataList<T, V> : ScriptableObject where V : DataList<T, V> where T : new()
{
    public List<T> Datas = new List<T>();
    public static V _self;
    public static V g_Self
    {
        get
        {
            //lazy loading
            if (_self == null)
            {
                _self = CreateInstance<V>();
                _self.LazyLoad();
            }
            return _self;
        }
    }

    public virtual void LazyLoad()
    {
        _self = CreateInstance<V>();
    }
    public virtual void TidyList()
    {

    }

    public virtual void AddItem()
    {
        Datas.Add(new T());
    }
    public virtual void AddItem( T Item)
    {
        Datas.Add(Item);
    }
    public virtual void RemoveItem(int index)
    {
        Datas.RemoveAt(index);
    }

    public virtual Dictionary<uint, T> ToDictionary()
    {
        Dictionary<uint, T> dic = new Dictionary<uint, T>();
        uint i = 0;
        foreach (T item in Datas)
        {
            dic.Add(i, item);
            i++;
        }
        return dic;
    }

}
