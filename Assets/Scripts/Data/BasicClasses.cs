﻿
[System.Serializable]
public class ItemSpawnPair
{
    public ItemSpawnPair(uint xID, int weight)
    {
        XID = xID;
        Weight = weight;
    }
   public uint XID;
   public int Weight;
}
