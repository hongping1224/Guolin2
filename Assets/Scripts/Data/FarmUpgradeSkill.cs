﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class FarmUpgradeSkill
{
    public enum FarmUpgradeSkillType
    {
        AddFarmWidth,
        AddFarmHeight,
        ReduceMatureTime,
        IncreaseWaterLifeSpan,
        IncreaseFertiLifeSpan,
        ReduceWaterRegenTime,
        ReduceFertiRegenTime,
        AddPriceOfFruit,
        AddItemSpawnRate,
    }

    public static int FarmHeight()
    {
        FarmUpgradeSave save = PlayerDataManager.FarmUpgrade;
        int change = 0;
        for (int i = 0; i < save.XID.Length; i++)
        {
            if (save.Upgrades[i])
            {
                FarmUpgradeData data = GameDataManager.GetFarmUpgradeData(save.XID[i]);

                if (data.SkillType == FarmUpgradeSkillType.AddFarmHeight)
                {
                    change += data.value;
                }
            }
        }
        return change;
    }

    public static int FarmWeight()
    {
        FarmUpgradeSave save = PlayerDataManager.FarmUpgrade;
        int change = 0;
        for (int i = 0; i < save.XID.Length; i++)
        {
            if (save.Upgrades[i])
            {
                FarmUpgradeData data = GameDataManager.GetFarmUpgradeData(save.XID[i]);

                if (data.SkillType == FarmUpgradeSkillType.AddFarmWidth)
                {
                    change += data.value;
                }
            }
        }
        return change;
    }
    public static int ReducedMatureTime()
    {
        FarmUpgradeSave save = PlayerDataManager.FarmUpgrade;
        int change = 0;
        for (int i = 0; i < save.XID.Length; i++)
        {
            if (save.Upgrades[i])
            {
                FarmUpgradeData data = GameDataManager.GetFarmUpgradeData(save.XID[i]);

                if (data.SkillType == FarmUpgradeSkillType.ReduceMatureTime)
                {
                    change += data.value;
                }
            }
        }
        return change;
    }

    public static int IncreaseWaterLifeSpan()
    {
        FarmUpgradeSave save = PlayerDataManager.FarmUpgrade;
        int change = 0;
        for (int i = 0; i < save.XID.Length; i++)
        {
            if (save.Upgrades[i])
            {
                FarmUpgradeData data = GameDataManager.GetFarmUpgradeData(save.XID[i]);

                if (data.SkillType == FarmUpgradeSkillType.IncreaseWaterLifeSpan)
                {
                    change += data.value;
                }
            }
        }
        return change;
    }
    public static int IncreaseFertiLifeSpan()
    {
        FarmUpgradeSave save = PlayerDataManager.FarmUpgrade;
        int change = 0;
        for (int i = 0; i < save.XID.Length; i++)
        {
            if (save.Upgrades[i])
            {
                FarmUpgradeData data = GameDataManager.GetFarmUpgradeData(save.XID[i]);

                if (data.SkillType == FarmUpgradeSkillType.IncreaseFertiLifeSpan)
                {
                    change += data.value;
                }
            }
        }
        return change;
    }
    public static int ReduceWaterRegenTime()
    {
        FarmUpgradeSave save = PlayerDataManager.FarmUpgrade;
        int change = 0;
        for (int i = 0; i < save.XID.Length; i++)
        {
            if (save.Upgrades[i])
            {
                FarmUpgradeData data = GameDataManager.GetFarmUpgradeData(save.XID[i]);

                if (data.SkillType == FarmUpgradeSkillType.ReduceWaterRegenTime)
                {
                    change += data.value;
                }
            }
        }
        return change;
    }
    
   public static int ReduceFertiRegenTime()
    {
        FarmUpgradeSave save = PlayerDataManager.FarmUpgrade;
        int change = 0;
        for (int i = 0; i < save.XID.Length; i++)
        {
            if (save.Upgrades[i])
            {
                FarmUpgradeData data = GameDataManager.GetFarmUpgradeData(save.XID[i]);

                if (data.SkillType == FarmUpgradeSkillType.ReduceFertiRegenTime)
                {
                    change += data.value;
                }
            }
        }
        return change;
    }
    public static int AddPriceOfFruit()
    {
        FarmUpgradeSave save = PlayerDataManager.FarmUpgrade;
        int change = 0;
        for (int i = 0; i < save.XID.Length; i++)
        {
            if (save.Upgrades[i])
            {
                FarmUpgradeData data = GameDataManager.GetFarmUpgradeData(save.XID[i]);

                if (data.SkillType == FarmUpgradeSkillType.AddPriceOfFruit)
                {
                    change += data.value;
                }
            }
        }
        return change;
    }
    public static ItemSpawnPair[] AddItemSpawnRate(ItemSpawnPair[] config)
    {
        List<ItemSpawnPair> holder = new List<ItemSpawnPair>();
        //copy pair to list
        foreach (ItemSpawnPair pair in config)
        {
            holder.Add(pair);
        }
        FarmUpgradeSave save = PlayerDataManager.FarmUpgrade;
        for (int i = 0; i < save.XID.Length; i++)
        {
            if (save.Upgrades[i])
            {
                FarmUpgradeData data = GameDataManager.GetFarmUpgradeData(save.XID[i]);

                if (data.SkillType == FarmUpgradeSkillType.AddItemSpawnRate)
                {
                    bool added = false;
                    for(int j =0; j< config.Length; j++)
                    {
                        if(holder[j].XID ==data.target)
                        {
                            holder[j].Weight += data.value;
                            added = true;
                            break;
                        }
                    }
                    if(!added)
                    {
                        holder.Add(new ItemSpawnPair(data.XID, data.value));
                    }
                }
            }
        }
        return holder.ToArray();
    }
}
