﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public struct StringData
{
    public uint XID;
    public string eng;
    public string chs;
    public string cht;
}

public class StringDataList : DataList<StringData, StringDataList>
{
    public const string FilePath = @"Assets/Resources/"+ResourcesPath.DATA + "StringData.asset";
    public const string ResFilePath = ResourcesPath.DATA + "StringData";

    public override void LazyLoad()
    {
        _self = Resources.Load<StringDataList>(ResFilePath);
    }

    public override void TidyList()
    {
        List<StringData> tmp = new List<StringData>();
        foreach (StringData f in Datas)
        {
            if (f.XID != 0)
            {
                tmp.Add(f);
            }
        }
        Datas = tmp;
    }

    public override Dictionary<uint, StringData> ToDictionary()
    {
        Dictionary<uint, StringData> dic = new Dictionary<uint, StringData>();
        foreach (StringData f in Datas)
        {
            if (!dic.ContainsKey(f.XID))
            {
                dic.Add(f.XID, f);
            }
            else
            {
                Debug.LogWarning("key already exsit! key : " + f.XID);
            }
        }
        return dic;
    }

}