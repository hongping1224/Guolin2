﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class MiniGameSystem : MonoBehaviour {
    private MiniGameData _data;
    public MiniGameData Data {
        get {
            if (!initialized)
                Initial();
            return _data;
        }
    }
    public GridLayoutGroup Grid;
    private TileRenderer[][] Tiles;
    private bool initialized = false;

    public TileRenderer GetTileRenderer(int x, int y) {
        return Tiles[x][y];
    }

    public void Awake() {
        Initial();
    }

    public void Initial() {
        if (!initialized) {
            _data = MiniGameData.Self;
            //RoundPlate
            _data.InitRoundPlateIten();

            // Grid
            TileRenderer[][] tiles = new TileRenderer[7][];
            List<TileRenderer> locktile = new List<TileRenderer>();
            for (int i = 0; i < 7; i++) {
                tiles[i] = new TileRenderer[6];
                for (int j = 0; j < 6; j++) {
                    TileRenderer tile = TileRenderer.Create(Grid.transform);
                    Vector3 oldpos = tile.transform.localPosition;
                    tile.transform.localPosition = new Vector3(oldpos.x, oldpos.y, oldpos.z - i);
                    tiles[i][j] = tile;
                    locktile.Add(tile);
                }
            }
            //AssignUnlockTiles;
            int[] sXY = getStartXYIndexForGrid(_data.FarmWidth, _data.FarmHeight);
            Tiles = new TileRenderer[_data.FarmHeight][];
            Debug.Log(_data.FarmHeight + " " + _data.FarmWidth);
            int ti = 0, tj = 0;
            for (int i = 0; i < _data.FarmHeight; i++) {
                tj = 0;
                Tiles[i] = new TileRenderer[_data.FarmWidth];
                for (int j = 0; j < _data.FarmWidth; j++) {
                    Tiles[i][j] = tiles[i + sXY[0]][j + sXY[1]];
                    Tiles[i][j].ReconfigurePosition(_data.Field.GetTiles(i, j));
                    Tiles[i][j].OnClick += Tiles[i][j].ClickOnTile;
                    Tiles[i][j].SetEmptySprite();
                    locktile.Remove(Tiles[i][j]);
                    tj++;
                }
                ti++;
            }
            StartCoroutine(initLockTile(locktile));
        }
        initialized = true;
    }

    // A hack needed due to the 1 frame delay of grid to recalculate position
    private IEnumerator initLockTile(List<TileRenderer> locktile) {

        foreach (TileRenderer tile in locktile) {
            tile.SetLockSprite();
        }
        yield return new  WaitForEndOfFrame();
        
        foreach (TileRenderer tile in locktile) {
            tile.PlaceItemOnTile(LockItemRenderer.Create(tile, new LockItem()));
            tile.itemOnTile.Added();
            yield return new WaitForEndOfFrame();

        }
    }


    private int[] getStartXYIndexForGrid(int width, int height) {
        if (width > height) {
            int tmp = width;
            width = height;
            height = tmp;
        }
        #region thinking process
        //width will only <= Height
        /*   if (width == 4)
           {
                   return new int[] { 1, 1 };
               if (height == 4)
               {
                   return new int[] { 1, 1 };
               }
               else if (height == 5)
               {
                   return new int[] { 1, 1 };
               }
           }
           else if (width == 5)
           {
                   return new int[] { 1, 1 };
               if (height == 5)
               {
                   return new int[] { 1, 1 };
               }
               else if (height == 6)
               {
                   return new int[] { 1, 1 };
               }
           }
           else if (width == 6)
           {
                   return new int[] { 0, 0 };
               if (height == 6)
               {
                   return new int[] { 0, 0 };
               }
               else if (height == 7)
               {
                   return new int[] { 0, 0 };
               }
           } */
        #endregion 
        if (width != 6) {
            return new int[] { 1, 1 };
        } else {
            return new int[] { 0, 0 };
        }

    }


    int turn = 0;
    /// <summary>
    /// Next turn when item apply success will fire , go to next turn
    /// </summary>
    public void NextTurn() {
        ++turn;
        //  Debug.Log("Next Turn : " + turn);
        int w = _data.Field.Tiles.GetLength(0);
        int h = _data.Field.Tiles.GetLength(1);
        // loop trough all tileitem , then call tile item onturnend
        for (int i = 0; i < w; ++i) {
            for (int j = 0; j < h; ++j) {
                TileItem item = _data.Field.Tiles[i, j].Item;
                if (item != null) {
                    item.OnTurnEnd();
                }
            }
        }

        //randomly grow grass.
        int x = Random.Range(0, w - 1);
        int y = Random.Range(0, h - 1);
        Tile rdTile = _data.Field.Tiles[x, y];
        if (rdTile.Item == null) {
            int rd = Random.Range(0, 100);
            Debug.Log(rd);
            if (rd <= Data.BugSpawnRate) {
                // generate worm
                WormItem wi = new WormItem();
                rdTile.SetItem(wi);
            } else {
                GrassItem gi = new GrassItem();
                rdTile.SetItem(gi);
            }
        }
    }
}
