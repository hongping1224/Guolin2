﻿using UnityEngine;
using DG.Tweening;

public class RoundPlateRotate : BaseObject {
    public BoxCollider Sensing;
    private Vector3 clickedPosition;
    public bool isRotating;
    public RoundPlateItemRenderer[] Items;
    private float targetAngle;
    private const float LerpAngle = 0.2f;
    private float[] boundary;
    public int selectedIndex;

    void OnEnable() {
        Init();
        isRotating = false;
        boundary = calculateBoundary(Items.Length);
        selectedIndex = 0;
        repositionItem();
    }

    void OnDisable() {
        transform.DestroyChildren();
    }

    // Update is called once per frame
    void FixedUpdate() {
        if (isRotating) {
            if (Input.GetMouseButton(0)) {
                Vector3 positionNow = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                // GET CENTER TO LAST POSTION VECTOR 
                Vector2 from = (clickedPosition - transform.position);
                //GET CENTER TO NEW POSITION VECTOR
                Vector2 to = (positionNow - transform.position);
                //CALCULATE ANGLE BETWEEN VECTOR
                float angle = Mathf.DeltaAngle(Mathf.Atan2(from.y, from.x) * Mathf.Rad2Deg,
                                Mathf.Atan2(to.y, to.x) * Mathf.Rad2Deg);
                transform.Rotate(Vector3.forward, angle, Space.Self);
                clickedPosition = positionNow;

            } else {
                //Hack Require for not using the round plate item when end spinning
                Invoke("EndRotate", 0.1f);
            }
            correctItemRotation();
        } else {
            //Check if already To Target
            if (transform.eulerAngles.z != targetAngle) {
                //Lerp To Target
                if (Mathf.Abs(transform.eulerAngles.z - targetAngle) >= 0.2f) {
                    float now = transform.eulerAngles.z;
                    transform.eulerAngles = new Vector3(0, 0, Mathf.LerpAngle(now, targetAngle, LerpAngle));
                } else {
                    transform.eulerAngles = new Vector3(0, 0, targetAngle);
                }
                correctItemRotation();
            }
        }
    }

    private void correctItemRotation() {
        float rotationNow = transform.eulerAngles.z;
        for (int i = 0; i < Items.Length; i++) {
            Items[i].transform.localEulerAngles = new Vector3(0, 0, -rotationNow);
        }
    }
    const int radius = -110;
    private void repositionItem() {
        for (int i = 0; i < boundary.Length; i++) {
            Items[i].transform.localPosition = calculatePosition(i);
        }
    }

    private Vector3 calculatePosition(int index) {
        if (index == 0) {
            return new Vector3(0, radius, 0);
        }
        float angle = (boundary[index] + boundary[index - 1]) / 2;
        //RMB:Change Angle TO RAD
        float x = radius * Mathf.Sin(angle / 180 * Mathf.PI);
        float y = radius * Mathf.Cos(angle / 180 * Mathf.PI);
        return new Vector3(x, y, 0);
    }


    public void StartRotate(Vector2 StartMousePosition) {

        if (!isRotating) {
            isRotating = true;
            clickedPosition = StartMousePosition;
        }
    }

    public void EndRotate() {
        isRotating = false;
        //Round to nearest degree
        float anglenow = transform.eulerAngles.z;
        if (anglenow >= boundary[boundary.Length - 1] || anglenow <= boundary[0]) {
            targetAngle = 0;
            selectedIndex = 0;
            return;
        }
        for (int i = 1; i < boundary.Length; i++) {
            if (anglenow >= boundary[i - 1] && anglenow <= boundary[i]) {
                targetAngle = (boundary[i - 1] + boundary[i]) / 2;
                selectedIndex = i;
                break;
            }
        }
    }

    private float angleBetweenVector2(Vector2 vec1, Vector2 vec2) {
        Vector2 diference = vec2 - vec1;
        float sign = (vec2.y < vec1.y) ? -1.0f : 1.0f;
        return Vector2.Angle(Vector2.right, diference) * sign;
    }

    private float[] calculateBoundary(int count) {
        float[] boundary = new float[count];
        float gap = 360f / count;
        boundary[0] = gap / 2;
        for (int i = 1; i < count; i++) {
            boundary[i] = boundary[i - 1] + gap;
        }
        return boundary;
    }

    public RoundPlateItemRenderer GetSelectedItem() {
        if (selectedIndex < Items.Length) {
            return Items[selectedIndex];
        }
        return null;
    }

    public override void Init() {
        transform.DestroyChildren();
        if (MiniGameWindow.gSelf == null) {
            MiniGameWindow.gSelf = GameObject.FindGameObjectWithTag("MiniGame").GetComponent<MiniGameWindow>();
        }
        RoundPlateItem[] items = MiniGameWindow.gSelf.Calculator.Data.GetItems();
        //TODO Change to ItemPool Load;
        int i = 0;
        Items = new RoundPlateItemRenderer[items.Length];
        foreach (RoundPlateItem item in items) {
            RoundPlateItemRenderer render = RoundPlateItemRenderer.Create(transform, item);
            //  render.transform.localScale = Vector3.one;
            Items[i] = render;
            i++;
        }
    }

    public void ChangeItem(int index, RoundPlateItem item) {
        //Debug.Log("Changes Done");
        RoundPlateItemRenderer nitem = RoundPlateItemRenderer.Create(transform, item);
        RoundPlateItemRenderer olditem = Items[index];
        Vector3 pos = calculatePosition(index);
        Items[index] = nitem;
        //animation
        olditem.transform.DOScale(Vector3.zero, 0.5f);
        nitem.transform.DOLocalMove(pos, 0.5f);
        olditem.DestroyAfter(0.51f);
        //swap the renderer ,  old renderer scale to 0 then destroy
        // new renderer pop out in the middle then go to its spot
    }

}