﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using Random = UnityEngine.Random;
public class TileRenderer : BaseObject {
    /*   private int x;
       private int y;*/
    public Image TileSprite;
    private static Transform tileItemCast;
    private static Transform GetCast() {
        if (tileItemCast == null) {
            tileItemCast = MiniGameWindow.gSelf.ItemCast;
        }
        return tileItemCast;
    }
    /*  public int X { get { return x; } }
      public int Y { get { return y; } }*/
    public TileItemRenderer itemOnTile { get; private set; }
    public event Action<Tile> OnClick;
    public Tile tile { get; protected set; }
    public Transform ItemCast;

    private static string[] grassSprite = new string[] { "grass_applegreen", "grass_bluegreen", "grass_darkgreen", "grass_green", "grass_yellow" };
    private static string[] groundSprite = new string[] { "ground_brown", "ground_darkbrown", "ground_redbrown" };

    public static TileRenderer Create(Transform parent) {
        TileRenderer tc = CreateObj<TileRenderer>(parent, "UI/Object/MiniGame/Tile");
        tc.ItemCast.SetParent(GetCast());
        tc.ItemCast.localScale = Vector3.one;
        tc.gameObject.name = "lock";
        tc.ItemCast.name = "lock";
        tc.itemOnTile = null;
       

      //tc.OnClick += tc.TileClicked;
        
        return tc;
    }

    public void ReconfigurePosition(Tile tile) {
        this.tile = tile;
        tile.OnItemChanges += OnTileItemChange;
        /* this.x = x;
         this.y = y;*/
        gameObject.name = tile.X + "_" + tile.Y;
        ItemCast.name = tile.X + "_" + tile.Y;
    }

    public void Click() {
        if (OnClick != null) {
            if (name != "lock") {
                OnClick(tile);
            } else {
                OnClick(null);
            }
        }
    }

    // this tile is the tile of the data field , not screen field;
    // lock tile will pass null 
    private void TileClicked(Tile t) {
        // do something
     
    }

    public void PlaceItemOnTile(TileItemRenderer item) {
        itemOnTile = item;
    }

    public void SetLockSprite() {
        if (Random.Range(0.0f, 1.0f) <= 0.7) {
            TileSprite.sprite = Resources.Load<Sprite>("Sprite/MiniGame/Tile/" + grassSprite[2]); // darkgreen
        } else {
            TileSprite.sprite = Resources.Load<Sprite>("Sprite/MiniGame/Tile/" + grassSprite[0]); // green
        }
    }

    public void SetEmptySprite() {
        if (Random.Range(0.0f, 1.0f) <= 0.7) {
            TileSprite.sprite = Resources.Load<Sprite>("Sprite/MiniGame/Tile/" + groundSprite[0]); // brown
        } else {
            TileSprite.sprite = Resources.Load<Sprite>("Sprite/MiniGame/Tile/" + groundSprite[1]); // darkbrown
        }
    }

    public void ClickOnTile(Tile t) {
        // play animation
        if (itemOnTile != null) {
            itemOnTile.ClickAnimation();
            //if Item OnClick Have Effect . Go NextTurn
            if (itemOnTile.OnClick()) {
                MiniGameWindow.gSelf.Calculator.NextTurn();
                return;
            }
        }
        
     
            //check Item Can Use 
            int selectedindex = MiniGameWindow.gSelf.Controller.Rotater.selectedIndex;
            RoundPlateItem item = MiniGameWindow.gSelf.Calculator.Data.GetItems()[selectedindex];
            TileItem tileItem = null;
            bool success = false;
            switch (item.Data.Tag) {
                case RoundPlateItemData.RoundPlateItemTag.Seed: {
                        FruitData fdata = GameDataManager.GetFruitData(item.Data.XID);
                        if (PlantItem.CheckUsable(t)) {
                            tileItem = new PlantItem(fdata);
                            //  PlaceItemOnTile(tileItem);
                            MiniGameWindow.gSelf.Calculator.Data.GenerateNextItem();
                            success = true;
                        }
                    }
                    break;
                case RoundPlateItemData.RoundPlateItemTag.Block: {
                        if (StumpItem.CheckUsable(t)) {
                            tileItem = new StumpItem();
                            MiniGameWindow.gSelf.Calculator.Data.GenerateNextItem();
                            success = true;
                        }
                    }
                    break;
                case RoundPlateItemData.RoundPlateItemTag.Water: {
                        if (WaterItem.CheckUsable(t)) {
                            WaterItem wi = new WaterItem(item.Data.XID, item.Data.amount);
                            t.UseItem(wi);
                            wi.GenerateEffect(this);
                            MiniGameWindow.gSelf.Calculator.Data.GenerateNextItem();
                            success = true;
                        }
                    }
                    break;
                case RoundPlateItemData.RoundPlateItemTag.Ferti: {
                        if (FertiItem.CheckUsable(t)) {
                            FertiItem wi = new FertiItem(item.Data.XID, item.Data.amount);
                            t.UseItem(wi);
                            wi.GenerateEffect(this);
                            MiniGameWindow.gSelf.Calculator.Data.GenerateNextItem();
                            success = true;
                        }
                    }
                    break;
                case RoundPlateItemData.RoundPlateItemTag.Showel: {
                        if (ShowelItem.CheckUsable(t)) {
                            ShowelItem wi = new ShowelItem(item.Data.XID);
                            t.UseItem(wi);
                            wi.GenerateEffect(this);
                            MiniGameWindow.gSelf.Calculator.Data.GenerateNextItem();
                            success = true;
                        }
                    }
                    break;
                default:
                    tileItem = null;
                    break;
            }

            t.SetItem(tileItem);

            // if item used is success then go to next turn
            if (success) {
                MiniGameWindow.gSelf.Calculator.NextTurn();
            }
    }

    public void OnTileItemChange(TileItem item) {
        TileItemRenderer render = item.GenerateRenderer(this);
        PlaceItemOnTile(render);
       // itemOnTile = render;
    }

}