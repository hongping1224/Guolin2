﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RoundPlateItemRenderer : BaseObject
{
    public Image Renderer;
    public RoundPlateItem Item { get; private set; }
    public void Init(RoundPlateItem item)
    {
        Item = item;
        Renderer.sprite = item.Data.Sprite;
    }
    public static RoundPlateItemRenderer Create(Transform parent, RoundPlateItem item) {
        RoundPlateItemRenderer render = CreateObj<RoundPlateItemRenderer>(parent, "UI/Object/MiniGame/RoundPlateItem");
        render.Init(item);
        return render;
    }
}
