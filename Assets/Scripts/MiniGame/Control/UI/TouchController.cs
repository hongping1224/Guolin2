﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class TouchController : BaseObject
{
    public RoundPlateRotate Rotater;
    //  private FieldClick target;
    //  public GhostObjRenderer ghost;
    float inputdelay = 0;
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Vector2 clickedPosition;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 1000f))
            {
                if (hit.collider.CompareTag("RotatePlate"))
                {
                    clickedPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Rotater.StartRotate(clickedPosition);
                }
                else if (hit.collider.CompareTag("Tile"))
                {
                    if (!Rotater.isRotating)
                    {
                        // Debug.Log(hit.collider.gameObject.name);
                        //     target = hit.collider.GetComponent<FieldClick>();
                        //Render Ghost on target
                        //     ghost.ShowOnTile(target.transform);
                    }
                }
            }
            else
            {
                //   target = null;
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {

            if (!Rotater.isRotating) {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 1000f)) {
                    if (hit.collider.CompareTag("Tile")) {

                        TileRenderer tr = hit.collider.GetComponent<TileRenderer>();
                        // Debug.Log(tr.name);
                        tr.Click();
                    }
                }
            }
            // ghost.HideGhost();
        }

    }
}

