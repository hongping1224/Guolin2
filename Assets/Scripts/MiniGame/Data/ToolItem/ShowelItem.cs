﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class ShowelItem : ToolItem {

    public ShowelItem(uint XID) {
        this.XID = XID;
    }

    public int Amount { get; protected set; }


    public override string ItemName { get { return "Showel"; } protected set { } }
    public override TileEffectRenderer GenerateEffect(TileRenderer OnTile) {
        ShowelEffectRenderer renderer = ShowelEffectRenderer.Create(OnTile.transform);
        renderer.PlayAnimation(OnTile);
        return renderer;
    }

    public static bool CheckUsable(Tile t) {
        if (t.Item != null &&
            (t.Item.GetType() == typeof(PlantItem) ||
            t.Item.GetType() == typeof(StumpItem) ||
            t.Item.GetType() == typeof(GrassItem)
            )) {
            return true;
        }
        return false;
    }

    public override void UseItem(Tile t) {
        t.RemoveItem();
    }

}
