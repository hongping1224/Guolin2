﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ToolItem {
    public virtual string ItemName { get; protected set; }
    public virtual uint XID { get; protected set; }
    public event Action<Tile> OnItemUsed;
    public void ItemUsed(Tile t) {
        UseItem(t);
       // Debug.Log("ItemUsed");
        if (OnItemUsed != null)
            OnItemUsed(t);
    }

    public virtual TileEffectRenderer GenerateEffect(TileRenderer OnTile) {
        return null;
    }

    public virtual void UseItem(Tile t){
       
    }

}
