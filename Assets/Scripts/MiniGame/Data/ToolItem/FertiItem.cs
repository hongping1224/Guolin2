﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class FertiItem : ToolItem {

    public FertiItem(uint XID, int Amount) {
        this.XID = XID;
        this.Amount = Amount;
    }

    public int Amount { get; protected set; }

    public override string ItemName { get { return "Ferti"; } protected set { } }
    public override TileEffectRenderer GenerateEffect(TileRenderer OnTile) {
        FertiEffectRenderer renderer = FertiEffectRenderer.Create(OnTile.transform);
        renderer.PlayAnimation(OnTile);
        return renderer;
    }

    public static bool CheckUsable(Tile t) {
        if (t.Item != null && t.Item.GetType() == typeof(PlantItem)) {
            if (((PlantItem)t.Item).statenow != PlantItem.plantState.Sprout) {
                return true;
            }
        }
        return false;
    }

    public override void UseItem(Tile t) {
        ((PlantItem)t.Item).turnOnField += Amount;
    }


}
