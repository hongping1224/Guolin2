﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class WaterItem : ToolItem {

    public WaterItem(uint XID , int Amount) {
        this.XID = XID;
        this.Amount = Amount;
    }

    public int Amount { get; protected set; }

    public override string ItemName { get { return "Water"; } protected set { } }
    public override TileEffectRenderer GenerateEffect(TileRenderer OnTile) {
        WaterEffectRenderer renderer = WaterEffectRenderer.Create(OnTile.transform);
        renderer.PlayAnimation(OnTile);
        return renderer;
    }
    public static bool CheckUsable(Tile t) {
        if (t.Item != null && t.Item.GetType() == typeof(PlantItem)) {
            if (((PlantItem)t.Item).statenow != PlantItem.plantState.Sprout) {
                return true;
            }
        }
        return false;
    }

    public override void UseItem(Tile t) {
        ((PlantItem)t.Item).turnOnField += Amount;
    }


}
