﻿using UnityEngine;
using System;


public class MiniGameData : SingletonBase<MiniGameData>
{
    private FruitGirl Setting { get; set; }
    private MiniGameData()
    {
    }
    public int WaterRegenTime { get { return Setting.WaterRegenTime; } }
    public int FertiRegenTime { get { return Setting.FertiRegenTime; } }
   // public int WaterLifeSpan { get { return config.WaterLifeSpan; } }
  //  public int FertilizerLifeSpan { get { return config.FertilizerLifeSpan; } }
    public int WaterGrowRate { get { return Setting.WaterGrowRate; } }
    public int FertiGrowRate { get { return Setting.FertiGrowRate; } }
    public int OtherGrowRate { get { return Setting.OtherGrowRate; } }
    public int BugSpawnRate { get { return Setting.BugSpawnRate; } }
    public int LinkNum { get { return Setting.LinkNum; } }
    public int ItemQueueNum { get { return Setting.ItemQueueNum; } }
    public int FruitMatureTime { get { return Setting.FruitMatureTime; } }
 //   public int FruitSproutTime { get { return config.FruitSproutTime; } }
    public int FarmWidth { get { return Setting.FarmWidth; } }
    public int FarmHeight { get { return Setting.FarmHeight; } }
    public ItemSpawnPair[] ItemChain { get { return Setting.ItemChain; } }

    public Field Field;
    private RoundPlateItem[] Items;
    public RoundPlateItem[] GetItems() {
        return Items;
    }
    public int[] DP;
    public override bool Initial()
    {
        Setting = FruitGirl.Generate(PlayerDataManager.SaveData.EquipingFruitGirl);
        Field = new Field(FarmWidth,FarmHeight);
        DP = new int[ItemChain.Length];
        DP[0] = ItemChain[0].Weight;
        for (int i= 1; i< ItemChain.Length; i++)
        {
            DP[i] = DP[i - 1] + ItemChain[i].Weight;
        }
        Items = new RoundPlateItem[ItemQueueNum];
        return true;
    }

    public event Action<int, RoundPlateItem> OnItemSet;

    public void SetItem(int index, RoundPlateItem item) {
        // do a check if item generate is legit 
        if (checkRotateCanHaveItem(index, item)) {
            // Debug.Log("set item");
            if (OnItemSet != null)
                OnItemSet(index, item);
            Items[index] = item;
        } else {
            // regenerate an item
            SetItem(index, generateNextItem());
        }
    }

    //generate a new item
    private RoundPlateItem generateNextItem() {
        int random = UnityEngine.Random.Range(0, MaxWeight);
        int index = GetIndexByWeight(random);
        return new RoundPlateItem(ItemChain[index].XID);
    }

    public void InitRoundPlateIten() {
        //RoundPlate
        for (int i = 0; i < GetItems().Length; ++i) {
            SetItem(i, generateNextItem());
        }
    }

    // put new item to the rotater
    public void GenerateNextItem() {
        SetItem(MiniGameWindow.gSelf.Controller.Rotater.selectedIndex, generateNextItem());
    }

    // make sure there are at least 1 item not all tool
    private bool checkRotateCanHaveItem(int index, RoundPlateItem item) {
        // if generate item not tool , then is ok
        if (!item.Data.IsTool()) {
            return true;
        }

        // if generated item is tool , check all item
        for (int i = 0; i < Items.Length; ++i) {
            if (index == i || Items[i] == null) { continue; }
            // if there is a item not tool then ok
            if (!Items[i].Data.IsTool()) {
                return true;
            }
        }
        //all item is tool ,cannot , need to be regenerate
        return false;
    }
    


    public int GetIndexByWeight(int weight)
    {
        if(weight <= DP[DP.Length -1])
        {
            for(int i=0; i<DP.Length; ++i)
            {
                if(weight<= DP[i])
                {
                    return i;
                }
            }
        }
        return 0;
    }

    public int MaxWeight { get { return DP[DP.Length - 1]; } }

 
}
