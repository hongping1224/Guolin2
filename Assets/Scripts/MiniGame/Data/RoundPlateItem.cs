﻿using UnityEngine;
using System.Collections;

public class RoundPlateItem
{
    public RoundPlateItemData Data{ get; private set;}
    public RoundPlateItem(uint XID)
    {
        Data = GameDataManager.GetRoundPlateItemData(XID);
    }
}
