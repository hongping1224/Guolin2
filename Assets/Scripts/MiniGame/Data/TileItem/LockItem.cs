﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockItem : TileItem {
    public override string ItemName { get { return "lock"; } protected set {  } }
    public override uint XID { get { return 0; } protected set { } }

    public override TileItemRenderer GenerateRenderer(TileRenderer OnTile) {
        return LockItemRenderer.Create(OnTile,this);
    }

}
