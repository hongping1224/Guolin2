﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrassItem : TileItem {

    public override string ItemName { get { return "grass"; } protected set { } }
    public override uint XID { get { return 100; } protected set { } }
    public override TileItemRenderer GenerateRenderer(TileRenderer OnTile) {
        return GrassItemRenderer.Create(OnTile, this);
    }
}
