﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class TileItem  {
    public virtual string ItemName { get;  protected set; }
    public virtual uint XID { get; protected set; }
    public virtual int turnOnField { get;  set; }
    public Tile OnTile;
    public event Action OnAdded;
    public event Action OnRemove;
    public event Action OnNextTurn;
    public virtual void OnTurnEnd() {
        turnOnField++;
        if (OnNextTurn != null)
            OnNextTurn();
    }

    public virtual TileItemRenderer GenerateRenderer(TileRenderer OnTile) {
        return null;
    }

    public void Remove() {
        if (OnRemove != null)
            OnRemove();
    }
    public void Added() {
        if (OnAdded != null)
            OnAdded();
    }
   


  
}
