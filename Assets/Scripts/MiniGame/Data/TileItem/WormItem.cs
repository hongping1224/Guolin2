﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WormItem : TileItem {
    public override string ItemName { get { return "worm"; } protected set { } }
    public override uint XID { get { return 999; } protected set { } }
    public override TileItemRenderer GenerateRenderer(TileRenderer OnTile) {
        return WormItemRenderer.Create(OnTile, this);
    }
    private bool ate = false;
    private int ateTurn = 0;

    public WormItem() {
    }

    public static bool CheckUsable(Tile t) {
        return false;
    }

    public enum WormState {
        egg,
        worm,
        cacoon,
        butterfly,
    }

    public WormState statenow {
        get {
            if (!ate) {
                if (turnOnField < 10) {
                    return WormState.egg;
                } else {
                    return WormState.worm;
                }
            } else {
                if(turnOnField - ateTurn < 10) {
                    return WormState.cacoon;
                } else {
                    return WormState.butterfly;
                }
            }
        }
    }
}
