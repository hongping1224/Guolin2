﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class PlantItem : TileItem {

    public FruitData fruitdata;
    public Action OnGrowth;

    public override string ItemName { get { return fruitdata.ClassName; } protected set { } }
    public override uint XID { get { return fruitdata.XID; } protected set { } }
    public plantState statenow {
        get {
            //    return plantState.Sprout;
            if (turnOnField <= MiniGameWindow.gSelf.Calculator.Data.FruitMatureTime) {
                return plantState.Seed;
            } else {
                return plantState.Sprout;
            }
        }
    }
    public enum plantState {
        Seed = 0,
        Sprout = 1,
    }
    public static bool CheckUsable(Tile t) {
        if (t.Item == null) {
            return true;
        }else if(t.Item.ItemName == "grass") {
            return true;
        }

        return false;
    }

    public PlantItem(FruitData data) {
        fruitdata = data;
    }

    private plantState oldState;
    public override void OnTurnEnd() {
        base.OnTurnEnd();
        // check state changes
        if(statenow != oldState) {
            // from seed to 
            if(oldState == plantState.Seed) {
                //sprout
                if (statenow == plantState.Sprout) {
                    //growth!
                    oldState = statenow;
                    if (OnGrowth != null)
                        OnGrowth();
                }
            }
        }
    }
    public override TileItemRenderer GenerateRenderer(TileRenderer OnTile) {
        PlantItemRenderer render = (PlantItemRenderer)PlantItemRenderer.Create(OnTile, this);
        return render;
    }
   
}
