﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StumpItem : TileItem {
    public override string ItemName { get { return "stump"; } protected set { } }
    public override uint XID { get { return 101; } protected set { } }
    public override TileItemRenderer GenerateRenderer(TileRenderer OnTile) {
        return StumpItemRenderer.Create(OnTile, this);
    }
    public static bool CheckUsable(Tile t) {
        if (t.Item == null) {
            return true;
        } else if (t.Item.ItemName == "grass") {
            return true;
        }

        return false;
    }
}
