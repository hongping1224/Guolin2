﻿using UnityEngine;
using System.Collections;
using System;
public class Tile
{

    public int X { get; private set; }// corr to w
    public int Y { get; private set; }// corr to h

    public TileItem Item { get; private set; }
    public Action<TileItem> OnItemChanges;
    public Tile(int x, int y)
    {
        X = x;
        Y = y;
    }


    public void SetItem(TileItem item)
    {
        if (item != null) {
            if (OnItemChanges != null)
                OnItemChanges(item);

            if (Item != null) {
                Item.Remove();
            }
            Item = item;
            Item.OnTile = this;
            Item.Added();
        }
    }

    public void RemoveItem() {
        if (Item != null) {
            Item.Remove();
        }
        Item = null;
    }

    public void UseItem(ToolItem item) {
        item.ItemUsed(this);
    }

}
