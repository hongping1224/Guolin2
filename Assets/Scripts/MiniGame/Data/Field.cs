﻿using UnityEngine;
using System.Collections;

public class Field
{
    public int Width { get; private set; }
    public int Height { get; private set; }
    public Tile[,] Tiles { get; private set; }
    public Field(int w,int h)
    {
        Width = w;
        Height = h;
        Tiles = new Tile[w, h];
        for (int y = 0; y < Height; y++)
        {
            for (int x = 0; x < Width; x++)
            {
                Tiles[x, y] = new Tile(x, y);
            }
        }
    }

    public Tile GetTiles(int x, int y)
    {
        if (x < Width && y < Height)
            return Tiles[x, y];

        Debug.LogWarning("Tile Out Of Range x:" + x + " y:" + y);

        return null;
    }


}
