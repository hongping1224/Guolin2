﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class StumpItemRenderer : TileItemRenderer<StumpItem> {
    public override void Init() {
        base.Init();
    }
    public override void Added() {
        transform.localScale = Vector3.zero;
        transform.DOScale(Vector3.one, 0.5f);
    }
}
