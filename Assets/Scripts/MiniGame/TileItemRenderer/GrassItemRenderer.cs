﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class GrassItemRenderer : TileItemRenderer<GrassItem> {
  
    public override void Init() {
        base.Init();
    }

    public override void Added() {
        transform.localScale = Vector3.zero;
        transform.DOScale(Vector3.one, 0.5f);
    }
}