﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FertiEffectRenderer : TileEffectRenderer {

    public static FertiEffectRenderer Create(Transform parent) {
        FertiEffectRenderer wr = CreateObj<FertiEffectRenderer>(parent.transform, "UI/Object/MiniGame/ToolItem/FertiEffect");
        var pos = wr.transform.localPosition;
        pos.y = 30;
        wr.transform.localPosition = pos;
        return wr;
    }

    public override void PlayAnimation(TileRenderer OnTile) {
        Debug.Log("Play");
        transform.DOLocalMoveY(-20f, 0.5f);
        DestroyAfter(0.5f);
    }
}
