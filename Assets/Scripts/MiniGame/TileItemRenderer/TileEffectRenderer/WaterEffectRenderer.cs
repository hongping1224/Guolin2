﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class WaterEffectRenderer : TileEffectRenderer {

    public static WaterEffectRenderer Create(Transform parent) {
        WaterEffectRenderer wr = CreateObj<WaterEffectRenderer>(parent.transform, "UI/Object/MiniGame/ToolItem/WaterEffect");
       var pos = wr.transform.localPosition;
        pos.y = 30;
        wr.transform.localPosition = pos;
        return wr;
    }

    public override void PlayAnimation(TileRenderer OnTile) {
        Debug.Log("Play");
        transform.DOLocalMoveY(-20f, 0.5f);
        DestroyAfter(0.5f);
    }
}
