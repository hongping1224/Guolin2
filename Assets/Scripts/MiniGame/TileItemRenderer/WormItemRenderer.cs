﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class WormItemRenderer : TileItemRenderer<WormItem> {
    public WormActor Worm;
    private static Vector3 wormposition = new Vector3(0, -40, -10);
    public override void Init() {
        base.Init();
        Worm = WormActor.Create(transform);
        Worm.transform.localPosition = wormposition;
    }
    public override void ClickAnimation() {
        if (Data.statenow == WormItem.WormState.worm) {
            Worm.PlayAnimation(WormAnimation.walkOnce);
        } 
    }

    public override void Added() {
        transform.localScale = Vector3.zero;
        transform.DOScale(Vector3.one, 0.5f);
    }
    

}
