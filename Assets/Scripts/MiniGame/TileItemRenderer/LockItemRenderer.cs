﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;
public class LockItemRenderer : TileItemRenderer<LockItem>
{
   public static string[] SpriteName = { "patch", "patch", "patch", "small tree log" , "small tree log", "small tree log", "stone_1" , "stone_2", "stone_3" };

    public override void Init() {
        base.Init();
        int invert = Random.Range(0, 2) * 2 - 1;
        transform.localScale = new Vector3(invert, 1, 1);
        Sprite.sprite = Resources.Load<Sprite>("Sprite/MiniGame/TileItem/" + SpriteName[Mathf.RoundToInt(Random.Range(0, SpriteName.Length))]);
    }

    public override void Added() {
        Vector3 scalenow = transform.localScale;
        transform.localScale = Vector3.zero;
        transform.DOScale(scalenow, 0.3f);
    }


}
