﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using DG.Tweening;

public class TileItemRenderer : BaseObject {
    public Image Sprite;
   
    public virtual void Remove() {
        transform.DOScale(Vector3.zero, 0.3f);
        DestroyAfter(0.31f);
    }
    public virtual void Added() {
         transform.DOScale(Vector3.one, 0.3f);
    }
    public virtual void OnNextTurn() {

    }
    //return true if item onclick need go to next turn 
    //Ex Plant Upgrade
    public virtual bool OnClick() {
        return false;
    }

    public virtual void ClickAnimation() {
        pull();
        Invoke("returnNormal", 0.21f);
    }

    public override void Init() {
        base.Init();
    }

    protected  void pull() {
        transform.DOScaleY(1.1f, 0.2f);
        transform.DOScaleX(Mathf.RoundToInt(transform.localScale.x) * 0.9f, 0.2f);
    }
    protected void returnNormal() {
        transform.DOScaleY(1.0f, 0.2f);
        transform.DOScaleX(Mathf.RoundToInt(transform.localScale.x) * 1.0f, 0.2f);
    }
    public void MoveTo(TileRenderer t,float duration = 0.5f) {
        transform.DOMove(t.transform.position, duration);
    }
}


public class TileItemRenderer<T> : TileItemRenderer where T : TileItem {
    public T Data { get; protected set; }
    public string ItemName { get { return Data.ItemName; } }
    public uint XID { get { return Data.XID; } }
    public int turnOnField { get { return Data.turnOnField; } }

    public static string GetPath() {
        Type ty = typeof(T);
        if (ty == typeof(LockItem))
            return "UI/Object/MiniGame/TileItem/LockItem";
        if (ty == typeof(PlantItem))
            return "UI/Object/MiniGame/TileItem/PlantItem";
        if (ty == typeof(GrassItem))
            return "UI/Object/MiniGame/TileItem/GrassItem";
        if (ty == typeof(StumpItem))
            return "UI/Object/MiniGame/TileItem/StumpItem";
        if (ty == typeof(WormItem))
            return "UI/Object/MiniGame/TileItem/WormItem";
        return "";
    }
    public override void Init() {
        base.Init();
        Data.OnAdded += Added;
        Data.OnRemove += Remove;
        Data.OnNextTurn += OnNextTurn;
    }
    public static TileItemRenderer<T> Create(TileRenderer OnTile, T item) {
        TileItemRenderer<T> LI = CreateObj<TileItemRenderer<T>>(OnTile.ItemCast, GetPath(), false);
        LI.Data = item;
        LI.Init();
        if (OnTile != null) {
            LI.transform.position = OnTile.transform.position;
        }
        return LI;
    }

}
