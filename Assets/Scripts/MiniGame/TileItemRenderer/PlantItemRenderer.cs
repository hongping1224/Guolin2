﻿using UnityEngine;
using System;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Spine.Unity;
using DG.Tweening;

public class PlantItemRenderer : TileItemRenderer<PlantItem> {
    public FruitActor fruit;
    public SeedActor seed;
    private static Vector3 fruitscale = new Vector3(0.35f, 0.35f, 1);
    private static Vector3 fruitposition = new Vector3(0, -40, -10);
    private static Vector3 seedposition = new Vector3(0, -40, -10);
    public Text TurnLeft;

    public override void Init() {
        base.Init();
        seed = SeedActor.Create(transform, Data.fruitdata);
        seed.transform.localPosition = seedposition;
        fruit = FruitActor.Create(transform, Data.fruitdata);
        fruit.transform.localScale = Vector3.zero;
        fruit.transform.localPosition = fruitposition;
        Data.OnGrowth += PlantGrowth;
        TurnLeft.text = (MiniGameWindow.gSelf.Calculator.Data.FruitMatureTime - Data.turnOnField+1).ToString();
        TurnLeft.transform.SetParent(MiniGameWindow.gSelf.FrontTextCast);
        StartCoroutine(SetTurnLeftPosition());
    }
    
    private IEnumerator SetTurnLeftPosition() {
        yield return new WaitForEndOfFrame();
        TurnLeft.transform.position = transform.position;
    }


    public override void OnNextTurn() {
        base.OnNextTurn();
        if (Data.statenow != PlantItem.plantState.Sprout) {
            TurnLeft.text = (MiniGameWindow.gSelf.Calculator.Data.FruitMatureTime - Data.turnOnField + 1).ToString();
        } else {
            TurnLeft.text = "";
        }
    }

    public override bool OnClick() {
        // Find All Link Fruit 
        List<Tile> links = linkPlants();
        // can upgrade
        if (links.Count>= MiniGameWindow.gSelf.Calculator.Data.LinkNum) {
         FruitData now =  GameDataManager.GetFruitData(Data.XID);
            // All Move to here 
            foreach (Tile t in links) {
                MiniGameWindow.gSelf.Calculator.GetTileRenderer(t.X, t.Y).itemOnTile.transform.DOMove(transform.position, 0.5f);
                t.RemoveItem();
            }
            // add new fruit    
            if(now.EvolveTo != 0) {
                PlantItem PI = new PlantItem(GameDataManager.GetFruitData(now.EvolveTo));
                Data.OnTile.SetItem(PI);
            } else {
                //reach max 
            }
            return true;
        }
        return false;
    }

    private List<Tile> linkPlants() {
        if(Data.statenow != PlantItem.plantState.Sprout) {
            return new List<Tile>();
        }
        Queue<Tile> queue = new Queue<Tile>();
        List<Tile> list = new List<Tile>();
        int[,] dir = new int[4, 2] { { 0, 1 }, { 1, 0 }, { 0, -1 }, { -1, 0 } };
        int width = MiniGameWindow.gSelf.Calculator.Data.FarmWidth;
        int height = MiniGameWindow.gSelf.Calculator.Data.FarmHeight;
        bool[,] flag = new bool[width, height];
        for (int i =0; i< width; ++i) {
            for (int j = 0; j < height; ++j) {
                flag[i, j] = false;
            }
        }
        queue.Enqueue(Data.OnTile);
        while (queue.Count > 0) {
            Tile t = queue.Dequeue();
            list.Add(t);
            int x = t.X;
            int y = t.Y;
            flag[x, y] = true;
            for (int i = 0; i < dir.GetLength(0); ++i) {
                int tx, ty;
                tx = x + dir[i, 0];
                ty = y + dir[i, 1];
                if (tx < 0 || ty < 0 || tx >= width || ty >= height) {
                    continue;
                }
                if (flag[tx, ty]) {
                    continue;
                }
                Tile tt = MiniGameWindow.gSelf.Calculator.Data.Field.GetTiles(tx, ty);
                flag[tt.X, tt.Y] = true;
                if (tt.Item != null && tt.Item.XID == Data.XID && ((PlantItem)tt.Item).statenow == PlantItem.plantState.Sprout) {
                    queue.Enqueue(tt);
                }
            }

        }
        return list;
    }

    public override void ClickAnimation()
    {
        if(Data.statenow == PlantItem.plantState.Seed)
        {
         
        }
        else
        {
            fruit.PlayAnimation(FruitAnimation.happy);
        }
    }

    public void PlantGrowth() {
        seed.transform.DOScale(Vector3.zero, 0.5f);
        fruit.transform.DOScale(FruitActor.ScaleOf(fruitscale), 0.5f);
    }

    public override void Added() {
        transform.localScale = Vector3.zero;
        transform.DOScale(Vector3.one, 0.5f);
    }

    public void OnDestroy() {
        Destroy(TurnLeft);
    }
}
