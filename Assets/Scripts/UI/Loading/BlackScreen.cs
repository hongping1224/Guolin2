﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections;

public class BlackScreen : BaseObject
{

    public Image Sprite;
    private bool IsBlack;
    public CanvasGroup canvasGroup;
    // Use this for initialization
    void OnEnable()
    {
        if (Sprite == null)
            Sprite = GetComponent<Image>();
        RectTransform rect = Sprite.transform as RectTransform;
        rect.sizeDelta = MainWindow.gSelf.GetRootCanvasSize();
    }
    public void FadeIn()
    {
        FadeIn(0.25f);
    }
    public void FadeOut()
    {
        FadeOut(0.25f);
    }
    public void FadeIn(float time)
    {
        IsBlack = true;
        canvasGroup.DOFade(1.0f, time);
    }
    public void FadeOut(float time)
    {
        IsBlack = false;
        canvasGroup.DOFade(0.0f,time);
        disable(time + 0.05f);
    }
    private void disable(float delay)
    {
        Invoke("setDisable", delay);
    }
    private void setDisable()
    {
        if (!IsBlack)
            gameObject.SetActive(false);
    }

}
