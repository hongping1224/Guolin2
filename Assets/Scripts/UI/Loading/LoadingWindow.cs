﻿using UnityEngine;
using System;

public class LoadingWindow : BaseWindow<LoadingWindow>
{
    public bool IsLoading { get; private set; }
    public BlackScreen BlackScreen;

    public void LoadWin<T>(Transform parent , string Path,Action<T> OnFinish ,Action<int> OnLoading = null) where T : BaseWindow<T>
    {
        BlackScreen.gameObject.SetActive(true);
        BlackScreen.FadeIn();
        if (OnFinish != null)
        {
            OnFinish += (T win) =>
            {
                BlackScreen.FadeOut();
            };
        }else
        {
            OnFinish = (T win) =>
            {
                BlackScreen.FadeOut();
            };
        }

        StartCoroutine(CreateWinAsync<T>(parent,Path,Vector3.zero,OnFinish,OnLoading));
    }
}
