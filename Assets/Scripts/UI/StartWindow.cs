﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;

public class StartWindow : BaseWindow<StartWindow>
{
    public RectTransform BG;
    public RectTransform Logo;
    public RectTransform StartButton;
    public RectTransform Clouds;
    public const string Path = ResourcesPath.Window + "StartWindow";
    private bool StartPressed = false;
    private bool startShow = false;
    public Button FrontFruit;

    void Awake()
    {
        gself = this;
    }

    public static StartWindow Create()
    {
        return Create(MainWindow.gself.GetRootCanvas(), Path);
    }

    public override void Init() {
        if (isInit) {
            return;
        }
        base.Init();
        initFruit();
        startAnimation();
    }

    private void initFruit() {
        List<uint> unlocked = PlayerDataManager.FruitUnlock.GetAllUnlocked();
        FruitActor frontactor = FruitActor.Create(FrontFruit.transform, GameDataManager.GetFruitData(unlocked[Random.Range(0, unlocked.Count)]));
        Vector3 pos = frontactor.transform.localPosition;
        pos.y = -187;
        frontactor.transform.localPosition = pos;
        FrontFruit.onClick.AddListener(frontactor.PlayHappy);
    }

    private void startAnimation()
    {
        StartCoroutine(StartCloudMove());
        Invoke("logoIN", 0.5f);
        Invoke("logoUP", 1.2f);
        InvokeRepeating("StartPingPong", 1.9f, 1.2f);
    }

    private void logoIN()
    {
        Logo.DOScale(Vector3.one, 0.5f);
    }
    private void logoUP()
    {
        Logo.DOLocalMoveY(714, 0.5f);
    }
    private void StartButtonIN(float time = 0.5f)
    {
        StartButton.GetComponent<Image>().DOFade(1.0f, time);
        startShow = true;
    }

    private void StartButtonOut(float time = 0.5f)
    {
        StartButton.GetComponent<Image>().DOFade(0.0f, time);
        startShow = false;
    }

    private void StartPingPong()
    {
        if (!StartPressed)
        {
            if (startShow)
            {
                StartButtonOut(1.0f);
            }
            else
            {
                StartButtonIN(1.0f);
            }
        }
        else
        {
            CancelInvoke("StartPingPong");
        }
    }


    public void StartClicked()
    {
        Debug.Log("click");
        Tweener tweener = BG.DOLocalMoveY(382, 1f);
        tweener.OnComplete<Tweener>(() =>
        {
            StartButton.gameObject.SetActive(false);
            Logo.gameObject.SetActive(false);
        });
        StartPressed = true;
    }

    private IEnumerator StartCloudMove()
    {
            yield return new WaitForSeconds(0.2f);
        for (int i = 0; i < Clouds.childCount; ++i)
        {
            cloudMove(Clouds.GetChild(i).gameObject);
            yield return new WaitForSeconds(Random.Range(2f, 8f));
        }
    }

    private void cloudMove(GameObject cloud)
    {
        RectTransform rectTransform = cloud.GetRectTransform();
        if (Random.value > 0.5)
        {
            rectTransform.localScale = new Vector3(rectTransform.localScale.x * -1, 1);
        }

        if (StartPressed)
        {
            rectTransform.localPosition = new Vector3(1000, Random.Range(-336, -186), 0);
        }
        else
        {
            rectTransform.localPosition = new Vector3(1000, Random.Range(-250, 132), 0);
        }
        Tweener tweener = rectTransform.DOLocalMoveX(-800, Random.Range(15, 35));
        tweener.OnComplete<Tweener>(() =>
        {
           // Debug.Log("run");
            cloudMove(cloud);
        });
    }


    public void LoadMiniGame() {
        MiniGameWindow.CreateAsync((mini) => {
            mini.Init();
            DestroySelf();
        });
    }
}
