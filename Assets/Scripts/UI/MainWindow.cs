﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Main Window All Other Window will Under This Window
/// </summary>
public class MainWindow : BaseWindow<MainWindow>
{
    public Canvas MainCanvas;
    public Canvas EffectCanvas;
    public GameObject SingletonObject;
    public GameObject ItemPool;
    public GameObject Factory;
    public LoadingWindow Loading;
    public enum StartingWindow
    {
        Real =0 ,
        StartWindow = 1,
        MiniGameWindow =2,
    }
    public StartingWindow startingWindow;
    private bool Initialize = false;
    new public static MainWindow gSelf
    {
        get
        {
            if (gself == null)
            {
                gself = GameObject.FindGameObjectWithTag("Main").GetComponent<MainWindow>();
            }
            return gself;
        }
        set
        {
            gself = value;
        }
    }

    public void Awake()
    {
        if (!Initialize)
        {
            gself = this;
            // Init Game Data
            GameDataManager.Initial();
            // Init Save Data
            PlayerDataManager.Initial();
            Initialize = true;
        }
    }

    public void Start()
    {
        switch (startingWindow)
        {
            case StartingWindow.Real:
            case StartingWindow.StartWindow:
                StartWindow start = StartWindow.Create();
                start.Init();
                break;
            case StartingWindow.MiniGameWindow:
                MiniGameWindow.Create().Init();
                break;
        }

    }

    public Transform GetRootCanvas()
    {
        return MainCanvas.transform;
    }
    public Vector2 GetRootCanvasSize()
    {
        RectTransform rect = MainCanvas.transform as RectTransform;
        return rect.sizeDelta;
    }


    // Hack for none Mono to Start Coroutine
    public void RunCoorutine(IEnumerator Coroutine)
    {
        StartCoroutine(Coroutine);
    }
}
