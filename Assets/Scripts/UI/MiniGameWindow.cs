﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class MiniGameWindow : BaseWindow<MiniGameWindow> {
    public MiniGameSystem Calculator;
    public TouchController Controller;
    public Transform FrontTextCast;
    public Transform ItemCast;
    private FruitActor Actor;
    public Button ActorContainer;
    public const string Path = ResourcesPath.Window + "MiniGameWindow";
    public static MiniGameWindow Create() {
        return Create(MainWindow.gself.GetRootCanvas(), Path);
    }

    public static void CreateAsync(Action<MiniGameWindow> OnFinish, Action<int> OnLoading = null) {
        MainWindow.gself.Loading.LoadWin<MiniGameWindow>(MainWindow.gself.GetRootCanvas(), Path, OnFinish, OnLoading);
    }

    public override void Init() {
        if (isInit) {
            return;
        }
        base.Init();
      
        Debug.Log("Init");
        if (Calculator == null) {
            Calculator = GetComponent<MiniGameSystem>();
            Calculator.Initial();
        }
        Calculator.Data.OnItemSet += Controller.Rotater.ChangeItem;
        FruitData EquipingFruitData = GameDataManager.GetFruitData(PlayerDataManager.SaveData.EquipingFruitGirl);
       // Debug.Log(EquipingFruitData);
        Actor = FruitActor.Create(ActorContainer.transform, EquipingFruitData);
        Actor.Init();
        Actor.transform.localPosition = new Vector3(0,-150,0);
        ActorContainer.onClick.AddListener(Actor.PlayHappy);
       
    }
}
