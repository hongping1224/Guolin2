﻿using UnityEngine;
using System.Collections;

public class ResourcesPath
{
    public const string Actor = "Actor/";
    public const string DATA = "DATA/";
    public const string Window = "UI/Window/";
    public const string Object = "UI/Object/";
	
}
