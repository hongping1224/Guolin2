﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WormActor : ActorBase<WormAnimation, WormAnimationState> {

    public static WormActor Create(Transform parent) {
        WormActor FA = CreateObj<WormActor>(parent, "Actor/Worm/Worm");
        FA.transform.localScale = baseScale*50;
        return FA;
    }

    public override void Init() {
        //  Debug.Log(skeleton);
        _defaultState = WormAnimationState.Get(WormAnimation.idle);
        StartCoroutine(Play(_defaultState.Animation));
    }

   
}
