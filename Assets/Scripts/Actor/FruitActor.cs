﻿using UnityEngine;
using System.Collections;
using Spine.Unity;
public class FruitActor: ActorBase<FruitAnimation,FruitAnimationState>
{
    public static FruitActor Create(Transform parent, FruitData Data)
    {
        FruitActor FA = CreateObj<FruitActor>(parent, "Actor/"+Data.ClassName+"/"+ Data.ClassName);
        FA.transform.localPosition = new Vector3(0, 0, -10);
        FA.transform.localScale = baseScale;
        return FA;
    }

    public override void Init()
    {
      //  Debug.Log(skeleton);
        _defaultState = FruitAnimationState.Get(FruitAnimation.idle);
        StartCoroutine(Play(_defaultState.Animation));
    }

    public void PlayHappy() {
        Debug.Log("Play");
        PlayAnimation(FruitAnimation.happy);
    }
    
}