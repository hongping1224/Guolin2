﻿using UnityEngine;
using System.Collections.Generic;

public class FruitAnimationState : AnimationStateBase<FruitAnimation, FruitAnimationState>
{
    protected override Dictionary<FruitAnimation, FruitAnimationState> InitAnimations()
    {
        Dictionary<FruitAnimation, FruitAnimationState> animations = new Dictionary<FruitAnimation, FruitAnimationState>();
        animations.Add(FruitAnimation.idle, new FruitAnimationState
        {
            Animation = FruitAnimation.idle,
            Loop = true,
            Name = "idle"
        });
        animations.Add(FruitAnimation.happy, new FruitAnimationState
        {
            Animation = FruitAnimation.happy,
            Loop = false,
            Name = "happy"
        });
        animations.Add(FruitAnimation.sad, new FruitAnimationState
        {
            Animation = FruitAnimation.sad,
            Loop = false,
            Name = "sad"
        });
        return animations;

    }

}
