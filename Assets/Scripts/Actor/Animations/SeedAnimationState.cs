﻿using UnityEngine;
using System.Collections.Generic;

public class SeedAnimationState :AnimationStateBase<SeedAnimation, SeedAnimationState> {

    protected override Dictionary<SeedAnimation, SeedAnimationState> InitAnimations() {
        Dictionary<SeedAnimation, SeedAnimationState> animations = new Dictionary<SeedAnimation, SeedAnimationState>();
        animations.Add(SeedAnimation.red, new SeedAnimationState {
            Animation = SeedAnimation.red,
            Loop = true,
            Name = "red"
        });
        animations.Add(SeedAnimation.yellow, new SeedAnimationState {
            Animation = SeedAnimation.yellow,
            Loop = true,
            Name = "yellow"
        });
        return animations;

    }
}
