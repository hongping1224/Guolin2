﻿using UnityEngine;
using System.Collections.Generic;

public class AnimationStateBase<T,S> where S : AnimationStateBase<T,S> , new()
{
    public T Animation { get; protected set; }
    public bool Loop { get; protected set; }
    public string Name { get; protected set; }
    protected static Dictionary<T, S> animations;
    public static S Get(T state)
    {
        S aState;
        if (animations == null)
        {
            // this is a hack needed to go through static func cannot be override
            AnimationStateBase<T, S> gself = new S();
            animations = gself.InitAnimations();
         /*   foreach (T k in animations.Keys)
            {
                Debug.Log(k);
            }*/
        }
        if (animations.TryGetValue(state, out aState))
        {
            return aState;
        }
        throw new System.ArgumentException("there is no " + state + " in Dictionary!! , Please Fix It");
    }
    protected virtual Dictionary<T, S> InitAnimations()
    {
        Debug.Log("asd");
        return new Dictionary<T, S>();
    }

}
