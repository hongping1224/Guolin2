﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WormAnimationState : AnimationStateBase<WormAnimation, WormAnimationState> {

    protected override Dictionary<WormAnimation, WormAnimationState> InitAnimations() {
        Dictionary<WormAnimation, WormAnimationState> animations = new Dictionary<WormAnimation, WormAnimationState>();
        animations.Add(WormAnimation.evolve, new WormAnimationState {
            Animation = WormAnimation.evolve,
            Loop = false,
            Name = "evolve"
        });
        animations.Add(WormAnimation.idle, new WormAnimationState {
            Animation = WormAnimation.idle,
            Loop = true,
            Name = "idle"
        });
        animations.Add(WormAnimation.walking, new WormAnimationState {
            Animation = WormAnimation.walking,
            Loop = true,
            Name = "walking"
        });
        animations.Add(WormAnimation.walkOnce, new WormAnimationState {
            Animation = WormAnimation.walkOnce,
            Loop = false,
            Name = "walking"
        });
        return animations;

    }
}
