﻿using UnityEngine;
using System.Collections;
using Spine.Unity;
public class SeedActor : ActorBase<SeedAnimation, SeedAnimationState> {
   

    public static SeedActor Create(Transform parent, FruitData Data) {
        SeedActor FA = CreateObj<SeedActor>(parent, "Actor/" + Data.ClassName + "/" + Data.ClassName+"Seed");
        FA.transform.localPosition = new Vector3(0, 0, -10);
        FA.transform.localScale = baseScale;
        return FA;
    }
    public override void Init() {
        //  Debug.Log(skeleton);
        if (Random.Range(0f, 1f) > 0.5f) {
            _defaultState = SeedAnimationState.Get(SeedAnimation.red);
        } else {
            _defaultState = SeedAnimationState.Get(SeedAnimation.yellow);
        }
        StartCoroutine(Play(_defaultState.Animation));
    }

}
