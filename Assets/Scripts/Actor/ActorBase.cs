﻿using UnityEngine;
using System.Collections;
using Spine.Unity;
using System;
using System.Collections.Generic;

public class ActorBase<T,S> : BaseObject 
    where S: AnimationStateBase<T,S> ,new()
    where T : struct, IConvertible
{
    public SkeletonGraphic Grapic;
    protected S _currentState;
    protected S _defaultState;
    public static Vector3 baseScale = new Vector3(0.01f, 0.01f, 1);
    public override void Init()
    {
        base.Init();
      //  Debug.Log(skeleton.AnimationName);
    }

    public void PlayAnimation(T animation)
    {
        StartCoroutine(Play(animation));
    }

    protected virtual IEnumerator Play(T animation)
    {
        if (Grapic == null)
       //     if (skeleton == null)
        {
            throw new System.ArgumentNullException("Skeleton Not Found");
        }
        if (_currentState == null || !_currentState.Animation.Equals(animation))
        {
            _currentState = AnimationStateBase<T, S>.Get(animation);
            //  string animName = _currentState.Name;
            if (!_currentState.Loop)
            {
                // skeleton.state.SetEmptyAnimation(0, 0f);
                // skeleton.Skeleton.SetToSetupPose();
                Grapic.AnimationState.SetEmptyAnimation(0, 0f);
                Grapic.Skeleton.SetToSetupPose();
                //  yield return new WaitForSeconds(0.05f);
            }

            // var track = skeleton.state.SetAnimation(0, _currentState.Name, _currentState.Loop);
            var track = Grapic.AnimationState.SetAnimation(0, _currentState.Name, _currentState.Loop);
            if (!_currentState.Loop)
            {
                while (!track.IsComplete)
                {
                    yield return null;
                }
                yield return Play(_defaultState.Animation);
            }
        }
    }

    public static Vector3 ScaleOf(float x) {
        return new Vector3(baseScale.x * x, baseScale.y * x, baseScale.z * x);
    }
    public static Vector3 ScaleOf(Vector3 x) {
        return new Vector3(baseScale.x * x.x, baseScale.y * x.y, baseScale.z * x.z);
    }


}
