﻿using UnityEngine;
using System.Collections;

public class Apple : FruitGirl
{
    public override ItemSpawnPair[] ItemChain
    {
        get
        {
            return new ItemSpawnPair[] { new ItemSpawnPair(1,1) };
        }
    }
}
