﻿using UnityEngine;
using System;

public class FruitGirl {

    protected int _waterStackSize;
    protected int _fertiStackSize;
    protected int _waterRegenTime ;
    protected int _fertiRegenTime ;
    protected int _waterLifeSpan;
    protected int _fertilizerLifeSpan; 
    protected int _saveWaterGrowRate;
    protected int _saveFertiGrowRate;
    protected int _saveOtherGrowRate;
    protected int _saveBugSpawnRate;
    protected int _saveLinkNum;
    protected int _itemQueueNum;
    protected int _fruitMatureTime;
    protected int _fruitSproutTime;
    protected int _farmWidth;
    protected int _farmHeight;
    public event Action<int,uint> _comboCallBack;
    protected ItemSpawnPair[] _itemChain;

    //Fix me Read Data From Save
    public virtual void Ini()
    {
        Config cf = Config.g_Self;
        _waterStackSize = cf.WaterStackSize;
        _fertiStackSize = cf.FertiStackSize;
        _waterRegenTime = cf.WaterRegenTime - FarmUpgradeSkill.ReduceWaterRegenTime();
        _fertiRegenTime = cf.FertiRegenTime - FarmUpgradeSkill.ReduceFertiRegenTime();
        _waterLifeSpan = cf.WaterLifeSpan + FarmUpgradeSkill.IncreaseWaterLifeSpan();
        _fertilizerLifeSpan = cf.FertilizerLifeSpan + FarmUpgradeSkill.IncreaseFertiLifeSpan();
        _saveWaterGrowRate = cf.WaterGrowRate;
        _saveFertiGrowRate = cf.FertiGrowRate;
        _saveOtherGrowRate = cf.OtherGrowRate;
        _saveBugSpawnRate = cf.BugSpawnRate;
        _saveLinkNum = cf.LinkNum;
        _itemQueueNum = cf.ItemQueueNum;
        _fruitMatureTime = cf.FruitMatureTime - FarmUpgradeSkill.ReducedMatureTime();
        _fruitSproutTime = cf.FruitSproutTime;
        _farmWidth = cf.FarmWidth + FarmUpgradeSkill.FarmWeight();
        _farmHeight = cf.FarmHeight + FarmUpgradeSkill.FarmHeight();
        _itemChain = FarmUpgradeSkill.AddItemSpawnRate(cf.ItemChain);
    }

    public virtual ItemSpawnPair[] ItemChain
    {
        get { return _itemChain; }
    }
    public virtual int WaterStackSize
    {
        get { return _waterStackSize; }
    }

    public virtual int FertiStackSize
    {
        get { return _fertiStackSize; }
    }

    public virtual int WaterRegenTime
    {
        get { return _waterRegenTime; }
    }

    public virtual int FertiRegenTime
    {
        get { return _fertiRegenTime; }
    }

    public virtual int WaterGrowRate
    {
        get { return _saveWaterGrowRate; }
    }

    public virtual int FertiGrowRate
    {
        get { return _saveFertiGrowRate; }
    }

    public virtual int OtherGrowRate
    {
        get { return _saveOtherGrowRate; }
    }

    public virtual int GetCombineSeedNum(int comboNum)
    {
        return Mathf.FloorToInt(comboNum / LinkNum);
    }   

    public virtual int BugSpawnRate
    {
        get { return _saveBugSpawnRate; }
    }

    public virtual int LinkNum
    {
        get { return _saveLinkNum; }
    }

    public void ComboCallBack(int combo,uint FruitID)
    {
        if(_comboCallBack != null)
            _comboCallBack(combo,FruitID);
    }
    public virtual int ItemQueueNum
    {
        get { return _itemQueueNum; }
    }
    public virtual int FruitSpourtTime
    {
        get { return _fruitSproutTime; }
    }
    public virtual int FruitMatureTime
    {
        get { return _fruitMatureTime; }
    }
    public virtual int FarmWidth
    {
        get { return _farmWidth; }
    }
    public virtual int FarmHeight
    {
        get { return _farmHeight; }
    }

    public static FruitGirl Generate(uint i)
    {
        FruitGirl fg;
        FruitData fd = GameDataManager.GetFruitData(i);

        if (fd.XID != 0)
        {
            Debug.Log("FruitGirl Generated: " + fd.ClassName);
            fg = (FruitGirl)System.Reflection.Assembly.GetExecutingAssembly().CreateInstance(fd.ClassName);
            fg.Ini();
        }
        else
        {
            fg = new FruitGirl();
            fg.Ini();
        }
        return fg;

    }

}
